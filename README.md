## INTRODUCTION

This module provides an email notification system attached to a configurable set of entities. You can create default templates on the entity type and override these on the entity itself.

The primary use case for this module is:

- Configure on which entity types you wish to run conditional notifications.
- Use simple plugins to extend this module with ease.
- Use provided hooks for logging the success or failure of the notification.

## REQUIREMENTS

No other requirements.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Vist /admin/config/system/conditional-notification-settings to configure the entity types you wish to attach conditonal notification.

## MAINTAINERS

Current maintainers for Drupal 10:

- PAUL MRVIK (globexplorer) - https://www.drupal.org/u/globexplorer

## DOCUMENTATION

You can extend the modules with your own plugins. Documentation comming soon.

