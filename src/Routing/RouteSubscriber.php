<?php

namespace Drupal\conditional_notification\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for conditional notification routes.
 *
 * @see \Drupal\devel\Controller\EntityDebugController
 * @see \Drupal\devel\Plugin\Derivative\DevelLocalTask
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The router service.
   *
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $routeProvider;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $router_provider
   *   The router service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, RouteProviderInterface $router_provider) {
    $this->entityTypeManager = $entity_manager;
    $this->routeProvider = $router_provider;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($route = $this->getEntityLoadRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.conditional_notification_templates", $route);
      }
      if ($route = $this->getEntityTypeDefinitionRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.conditional_notification_default_templates", $route);
      }
    }        
  }     

  /**
   * Gets the entity load route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getEntityLoadRoute(EntityTypeInterface $entity_type): ?Route {
    if ($conditional_notification_templates = $entity_type->getLinkTemplate('conditional-notification-templates')) {
      
     
      $route = (new Route($conditional_notification_templates))
        ->addDefaults([
          '_controller' => '\Drupal\conditional_notification\Controller\ConditionalNotificationController::entityTemplates',
          '_title' => 'Notification Templates',
        ])
        ->addRequirements([
          '_permission' => 'administer conditional_notification',
          '_custom_access' => '\Drupal\conditional_notification\Controller\ConditionalNotificationController::entityTemplateAccess'
        ])
        ->setOption('_admin_route', TRUE)
        ->setOption('_conditional_notification_entity_type_id', $entity_type->id());
        //->setOption('_conditional_notification', TRUE);

      if ($parameters = $this->getRouteParameters($entity_type, 'canocial')) {
        $route->setOption('parameters', $parameters);
      }

      return $route;
    }
    return NULL;
  }

  /**
   * Gets the entity type definition route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getEntityTypeDefinitionRoute(EntityTypeInterface $entity_type): ?Route {

    if ($conditional_notification_default_templates = $entity_type->getLinkTemplate('conditional-notification-default-templates')) {

      $route = (new Route($conditional_notification_default_templates))
        ->addDefaults([
          '_controller' => '\Drupal\conditional_notification\Controller\ConditionalNotificationController::defaultTemplates',
          '_title' => 'Entity type definition',
        ])
        ->addRequirements([
          '_permission' => 'administer conditional_notification',
          '_custom_access' => '\Drupal\conditional_notification\Controller\ConditionalNotificationController::defaultTemplateAccess'
        ])
        ->setOption('_admin_route', TRUE)
        ->setOption('_conditional_notification_entity_type_id', $entity_type->id());
        //->setOption('_conditional_notification', TRUE);

      if ($parameters = $this->getRouteParameters($entity_type, 'edit-form')) {
        $route->setOption('parameters', $parameters);
      }

      return $route;
    }
    return NULL;
  }

  /**
   * Gets the route parameters from the template.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param string $link_template
   *   The link template.
   *
   * @return array[]
   *   A list of route of parameters.
   */
  protected function getRouteParameters(EntityTypeInterface $entity_type, string $link_template): array {
    $parameters = [];
    if (!$path = $entity_type->getLinkTemplate($link_template)) {
      return $parameters;
    }

    $original_route_parameters = [];
    $candidate_routes = $this->routeProvider->getRoutesByPattern($path);
    if ($candidate_routes->count()) {
      // Guess the best match. There could be more than one route sharing the
      // same path. Try first an educated guess based on the route name. If we
      // can't find one, pick-up the first from the list.
      $name = 'entity.' . $entity_type->id() . '.' . str_replace('-', '_', $link_template);
      if (!$original_route = $candidate_routes->get($name)) {
        $iterator = $candidate_routes->getIterator();
        $iterator->rewind();
        $original_route = $iterator->current();
      }
      $original_route_parameters = $original_route->getOption('parameters') ?? [];
    }

    if (preg_match_all('/{\w*}/', $path, $matches)) {
      foreach ($matches[0] as $match) {
        $match = str_replace(['{', '}'], '', $match);
        // This match has an original route parameter definition.
        if (isset($original_route_parameters[$match])) {
          $parameters[$match] = $original_route_parameters[$match];
        }
        // It could be an entity type?
        elseif ($this->entityTypeManager->hasDefinition($match)) {
          $parameters[$match] = ['type' => "entity:$match"];
        }
      }
    }

    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

}
