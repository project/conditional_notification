<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\conditional_notification\Helper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\conditional_notification\CnTemplateInterface;
use Drupal\conditional_notification\Event\ConditionInsertTriggerEvent;
use Drupal\conditional_notification\Event\ConditionUpdateTriggerEvent;
use Drupal\conditional_notification\Event\ConditionCronTriggerEvent;
use Drupal\conditional_notification\Event\RecipientsEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * @todo Add class description.
 */
final class NotificationFactory {

  use LoggerChannelTrait;

  /**
   * Constructs a NotificationFactory object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly RouteMatchInterface $routeMatch,
    private readonly AccountProxyInterface $currentUser,
    private readonly Helper $conditionalNotificationHelper,
    private readonly EventDispatcherInterface $eventDispatcher
  ) {}

  /**
   * @todo Add method description.
   */
  public function createDefaultNotifications(EntityInterface $entity): void {

    $conditions = [
      'entity_action_type' => 'insert',
      'notification_type' => 'default',
      'notification_entity_type'   => $entity->getEntityTypeId(),
      'notification_entity_bundle' => $entity->bundle(),    
    ];

    // Changed to cn_template
    $conditional_notifications = $this->entityTypeManager
      ->getStorage('cn_template')
      ->loadByProperties($conditions);

    if (!empty($conditional_notifications)) {
      foreach ($conditional_notifications as $notification) {

        if ($notification->getStatus()) {
          if (
            $notification->getNotificationEntityType() === $entity->getEntityTypeId() &&
            $notification->getNotificationEntityBundle() === $entity->bundle()          
          ) {

            $status = $notification->getStatus();

            if ($override_notification = $this->checkForNotificationOverride($notification, $entity, 'insert')) {
              $notification_id = $override_notification->id();
              $status = $override_notification->getStatus();              
            }   
            else {
              $notification_id = $notification->id();
            }

            if ($status) {
              // Get the selected condition now from ECA
              $condition_eca_model_machine_name = $notification->getNotificationConditionEca();
              $event = new ConditionInsertTriggerEvent($entity, $entity->getEntityTypeId(), $entity->bundle(), 'default', $notification_id, $condition_eca_model_machine_name);
              $this->eventDispatcher->dispatch($event, ConditionInsertTriggerEvent::EVENT_NAME); 
            }

          }
        }
      }    
    }  
  }

  /**
   * Check if we have an override that is set to inactive
   *
   * @param $notification
   *   The noticiation object.
   * 
   * @param $entity
   *   The entity object.
   * 
   * @return boolean
   *   TRUE | FALSE
   */
  protected function isActiveOverride($notification, $entity, $action_type) {
    $allow_sending = TRUE;
    if ($override_notification = $this->checkForNotificationOverride($notification, $entity, $action_type)) {
      if (!$override_notification->getStatus()) {
        $allow_sending = FALSE;
      }
    }
    return $allow_sending;
  }

  /**
   * Creates notifications
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @return void
   */
  public function createNotifications(EntityInterface $entity): void {

    $conditions = [
      'entity_action_type' => 'update',
      'notification_type' => 'default',
      'notification_entity_type'   => $entity->getEntityTypeId(),
      'notification_entity_bundle' => $entity->bundle(),    
    ]; 

    $conditional_notifications = $this->entityTypeManager
      ->getStorage('cn_template')
      ->loadByProperties($conditions);

    if (!empty($conditional_notifications)) {
      foreach ($conditional_notifications as $notification) {
        if ($notification->getStatus()) {
          if (
            $notification->getNotificationEntityType() === $entity->getEntityTypeId() &&
            $notification->getNotificationEntityBundle() === $entity->bundle()          
          ) {

            $status = $notification->getStatus();
 
            if ($override_notification = $this->checkForNotificationOverride($notification, $entity, 'update')) {
              \Drupal::logger('debug')->debug('Override Notification on Update: @data : @template', ['@data' => $override_notification->id(), '@template' => $override_notification->label()]);
              $notification_id = $override_notification->id();
              $status = $override_notification->getStatus(); 
            }
            else {
              $notification_id = $notification->id();
            }

            if ($status) {              
              $condition_eca_model_machine_name = $notification->getNotificationConditionEca(); 
              \Drupal::logger('debug')->info('ECA Conditional Notification on Update: @data : @template : @eca_model', ['@eca_model' => $condition_eca_model_machine_name, '@data' => $notification_id, '@template' => $notification->label()]);
              // ECA Condition              
              $event = new ConditionUpdateTriggerEvent($entity, $entity->getEntityTypeId(), $entity->bundle(), 'default', $notification_id, $condition_eca_model_machine_name);
              $this->eventDispatcher->dispatch($event, ConditionUpdateTriggerEvent::EVENT_NAME);
            }
          }        
        }
      }    
    }  
  }

  /**
   * Creates notifications on a cron job
   *
   * @param array $data
   *   The data to create notifications for.
   * @return void
   */
  public function createNotificationsOnCron($data): void {

    if (!empty($data)) {
      //$entity = $this->entityTypeManager->getStorage($data['entity_type'])->load($data['entity_id']);
      $this->addToQueue($data);
      // Set to processed as it now lives in queue.
      $this->setNotificationLogToProcessed($data);      
    }  
  }  

  /**
   * Set a notification log to processed
   *
   * @param array $data
   *   The data to create notifications for.
   * @return void
   */
  protected function setNotificationLogToProcessed($data) {

    $conditions = [
      'notification_id' => $data['notification_id'],
      'entity_id' => $data['entity_id'],
      'entity_type' => $data['entity_type'],
    ];

    try {

      $logs = $this->entityTypeManager->getStorage('conditional_notification_log')->loadByProperties($conditions);

      if ($log = reset($logs)) {
        if ($log instanceof EntityInterface) {
          $log->processed->value = 1;
          $log->processed_timestamp->value = time();
          $log->save();
        }
      }        

    }
    catch (\Exception $exception) {
      $message = $exception->getMessage();
      $debug = json_encode($conditions);      
      $this->getLogger('conditional_notification')
        ->warning('Problem updating conditional notification log: @message. Debug Info: @debug.', [
        	'@message' => $message,
        	'@debug' => $debug	
       	]);
    }
  }
  
  /**
   * Check if we neeed an override for a notification.
   *
   * @param $conditional_notification
   *   The conditional notification object.
   * @param $entity
   *   The entity object.
   * @return void
   */
  protected function checkForNotificationOverride($conditional_notification, $entity, $action_type) {

    $override = FALSE;

    $conditions = [
      'notification_id' => $conditional_notification->id(),
      'entity_action_type' => $action_type,
      'notification_type' => 'override',
      'entity_id' => $entity->id()
    ];

    \Drupal::logger('debug')->debug('Checking for Override: @data : @template', ['@data' => $entity->id(), '@template' => $conditional_notification->label()]);

    $conditional_notifications = $this->entityTypeManager
      ->getStorage('cn_template')
      ->loadByProperties($conditions);

    if ($conditional_notification = reset($conditional_notifications)) {
      $override = $conditional_notification; 
    }

    return $override; 

  }

  /**
   * Adds data to a queue for later processing.
   *
   * @param array $data
   * @return void
   */
  protected function addToQueue($data) { 
    $queue = \Drupal::queue('conditional_notification_notification_queue')->createItem($data);
  }

  /**
   * Adds data to create a log queue for later processing.
   *
   * @param array $data
   * @return void
   */
  protected function addToLogQueue($data) {
    $queue = \Drupal::queue('conditional_notification_notification_log_queue')->createItem($data);
  }  

  /**
   * Creates a notification log for easy processing,
   * on cron and avoiding querie over the whole entity table.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @return void
   */
  public function createNotificationLog(EntityInterface $entity) {

    $conditions = [
      'entity_action_type' => 'cron',
      'notification_type' => 'default',
      'notification_entity_type'   => $entity->getEntityTypeId(),
      'notification_entity_bundle' => $entity->bundle(),    
    ]; 

    $conditional_notifications = $this->entityTypeManager
      ->getStorage('cn_template')
      ->loadByProperties($conditions);    
      
    if (!empty($conditional_notifications)) {
      foreach ($conditional_notifications as $notification) {
        // Only process enabled notifications.
        if ($notification->getStatus()) {
          if (
            $notification->getNotificationEntityType() === $entity->getEntityTypeId() &&
            $notification->getNotificationEntityBundle() === $entity->bundle()          
          ) {         
            
            $status = $notification->getStatus();

            if ($override_notification = $this->checkForNotificationOverride($notification, $entity, 'cron')) {
              $status = $override_notification->getStatus();
              $notification_id = $override_notification->id();
              $data['notification_id'] = $override_notification->id();
              $data['entity_type'] = $entity->getEntityTypeId();
              $data['entity_id'] = $entity->id();
              $data['time_offset_enabled'] = $override_notification->getTimeOffsetEnabled();
              // Check field
              if ($date_field = $override_notification->getTimeOffsetDateFieldOnly()) {
                $data['entity_date_field'] = $date_field;
                if ($date_field_part = $override_notification->getTimeOffsetDateFieldPart()) {
                  $data['entity_date_field_part'] = $date_field_part;
                  $data['entity_date_field_timestamp'] = $entity->{$date_field}->{$date_field_part};
                }
                else {
                  $data['entity_date_field_part'] = NULL;
                  $data['entity_date_field_timestamp'] = $entity->get($date_field)->value; 
                }
                // Calculate offset
                $time_offset = $override_notification->getTimeOffset();
                $time_offset_type = $override_notification->getTimeOffsetType();
                $data['time_offset_type'] = $time_offset_type;
                $data['time_offset'] = $time_offset;
                $data['entity_date_offset'] = $data['entity_date_field_timestamp'] + ($this->getCalculatedOffset($time_offset, $time_offset_type));  
                $data['entity_date_string_day'] = $this->getCalculatedOffsetString($data['entity_date_offset']);
                $data['entity_date_string_hour'] = $this->getCalculatedOffsetString($data['entity_date_offset'], 'hours');     
              }
            }
            else {    
              $notification_id = $notification->id();
              $data['notification_id'] = $notification->id();
              $data['entity_type'] = $entity->getEntityTypeId();
              $data['entity_id'] = $entity->id();
              $data['time_offset_enabled'] = $notification->getTimeOffsetEnabled();
              // Check field
              if ($date_field = $notification->getTimeOffsetDateFieldOnly()) {
                $data['entity_date_field'] = $date_field;
                if ($date_field_part = $notification->getTimeOffsetDateFieldPart()) {
                  $data['entity_date_field_part'] = $date_field_part;
                  $data['entity_date_field_timestamp'] = $entity->{$date_field}->{$date_field_part};
                }
                else {                                      
                  $data['entity_date_field_part'] = NULL;
                  $data['entity_date_field_timestamp'] = $entity->get($date_field)->date->getTimestamp();                   
                }
                // Calculate offset
                $time_offset = $notification->getTimeOffset();
                $time_offset_type = $notification->getTimeOffsetType();
                $data['time_offset_type'] = $time_offset_type;
                $data['time_offset'] = $time_offset;
                $calculdated_offset = $this->getCalculatedOffset($time_offset, $time_offset_type);   
                $data['entity_date_offset'] = $data['entity_date_field_timestamp'] + ($calculdated_offset);  
                $data['entity_date_string_day'] = $this->getCalculatedOffsetString($data['entity_date_offset']);
                $data['entity_date_string_hour'] = $this->getCalculatedOffsetString($data['entity_date_offset'], 'hours');
              }
            }

            if ($status) {
              // Get the selected condition now from ECA
              $condition_eca_model_machine_name = $notification->getNotificationConditionEca();
              // Conditons now ECA Models
              $event = new ConditionCronTriggerEvent($entity, $entity->getEntityTypeId(), $entity->bundle(), 'default', $notification_id, $data, $condition_eca_model_machine_name);
              $this->eventDispatcher->dispatch($event, ConditionCronTriggerEvent::EVENT_NAME);  
            }            
          }
        }
      }
    }
  }

  /**
   * Get the calculation offset
   *
   * @param int $time_offset
   *   The given offset.
   * @param string $type
   *   If hour or day.
   * @return integer
   *   The postive or negative time offset.
   */
  protected function getCalculatedOffset($time_offset, $type) {

    $result = 0;

    if ($type === 'hours') {
      $result = $time_offset * 3600;      
    }
    elseif ($type === 'days') {
      $result = $time_offset * 86400;  
    }

    return $result;

  }  

  /**
   * Get calculated offset string.
   *
   * @param array $entity_date_offset
   *   The offest.
   * @param string $format
   *   The format in days, hours.
   * @return string 
   *   The formatted string.
   */
  protected function getCalculatedOffsetString($entity_date_offset, $format = 'days'):string {

    if ($format === 'hours') {
      // Hours format
      $formatted_string = \Drupal::service('date.formatter')->format(
        $entity_date_offset,
        'custom',
        'H'
      );
    }
    else {
      $formatted_string = $entity_date_string_day = \Drupal::service('date.formatter')->format(
        $entity_date_offset,
        'custom',
        'd.m.Y'
      );
    }

    return $formatted_string;

  }
  

}
