<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a conditional notification store entity type.
 */
interface CnTemplateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * {@inheritdoc}
   */
  public function getSubject();

  /**
   * {@inheritdoc}
   */
  public function setSubject(string $subject);

  /**
   * {@inheritdoc}
   */
  public function getBody();

  /**
   * {@inheritdoc}
   */
  public function getBodyFormat();

  /**
   * {@inheritdoc}
   */
  public function setBody(array $body);

  /**
   * {@inheritdoc}
   */
  public function getMailFrom();

  /**
   * {@inheritdoc}
   */
  public function setMailFrom(string $mail_from);

  /**
   * {@inheritdoc}
   */
  public function getMailReplyTo();

  /**
   * {@inheritdoc}
   */
  public function setMailReplyTo(string $mail_reply_to);

  /**
   * {@inheritdoc}
   */
  public function getMailFromEnabled();

  /**
   * {@inheritdoc}
   */
  public function setMailFromEnabled($mail_from_enabled);

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityType();

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityType(string $notification_entity_type);

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityBundle();

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityBundle(string $notification_entity_bundle);

  /**
   * {@inheritdoc}
   */
  public function getEntityActionType();

  /**
   * {@inheritdoc}
   */
  public function setEntityActionType(string $entity_action_type);
  
  /**
   * {@inheritdoc}
   */
  public function getNotificationId();

  /**
   * {@inheritdoc}
   */
  public function setNotificationId(string $notification_id);

  /**
   * {@inheritdoc}
   */
  public function getEntityId();

  /**
   * {@inheritdoc}
   */
  public function setEntityId(string $entity_id);

  /**
   * {@inheritdoc}
   */
  public function getNotificationType();

  /**
   * {@inheritdoc}
   */
  public function setNotificationType(string $notification_type);

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetEnabled();

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetEnabled($time_offset_enabled);

  /**
   * {@inheritdoc}
   */
  public function getTimeOffset();

  /**
   * {@inheritdoc}
   */
  public function setTimeOffset(int $time_offset);

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetType();

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetType(string $time_offset_type);

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateField();

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldOnly();

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateField($time_offset_date_field);

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldPart();

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateFieldPart($time_offset_date_field_part);

  /**
   * {@inheritdoc}
   */
  public function getNotificationConditionEca();

  /**
   * {@inheritdoc}
   */
  public function setNotificationConditionEca(string $notification_condition_eca);
  /**
   * {@inheritdoc}
   */
  public function getNotificationContextEca();

  /**
   * {@inheritdoc}
   */
  public function setNotificationContextEca(string $notification_context_eca);

}
