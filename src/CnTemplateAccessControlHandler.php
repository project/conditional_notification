<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the conditional notification store entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class CnTemplateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    return match($operation) {
      'view' => AccessResult::allowedIfHasPermission($account, 'view cn_template'),
      'update' => AccessResult::allowedIfHasPermission($account, 'edit cn_template'),
      'delete' => AccessResult::allowedIfHasPermission($account, 'delete cn_template'),
      'delete revision' => AccessResult::allowedIfHasPermission($account, 'delete cn_template revision'),
      'view all revisions', 'view revision' => AccessResult::allowedIfHasPermissions($account, ['view cn_template revision', 'view cn_template']),
      'revert' => AccessResult::allowedIfHasPermissions($account, ['revert cn_template revision', 'edit cn_template']),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create cn_template', 'administer cn_template'], 'OR');
  }

}
