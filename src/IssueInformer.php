<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Logger\LoggerChannelTrait;


/**
 * @todo Add class description.
 */
final class IssueInformer {

  use StringTranslationTrait;

  use LoggerChannelTrait;

  /**
   * Constructs a Helper object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly MailManagerInterface $mailManager,
    private readonly EmailValidatorInterface $emailValidator,
  ) {}
 
  
  /**
   * Check for a valid email
   *
   * @param string $email
   *   The email to check
   * @return boolean
   *   TRUE | FALSE
   */
  public function isEmailValid(string $email): bool {
    return $this->emailValidator->isValid($email);
  }

  function sendIssueInformerMail(array $debug) {

    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $validated_emails = FALSE;

    $config = $this->configFactory->get('conditional_notification.settings');

    if ($issue_informer_emails = $config->get('issue_informer_emails')) {
      // Get emails from field
      $emails = explode(';', trim($issue_informer_emails));
      // Get subject
      if ($subject = $config->get('issue_informer_subject')) {
        $email_subject = $subject;
      }
      else {
        $email_subject = t('Issue informer Conditional Notifcations!');
      }

      $context = [
        'subject' => $email_subject,
        'message' => json_encode($debug),
      ];      

      foreach ($emails as $email) {
        // Sending Email
        $delivered = $this->mailManager->mail('system', 'action_send_email', $email, $langcode, [
          'context' => $context
        ]);
      }

      if (!$delivered) {
        $this->getLogger('conditional_notification')->warning('A issue informer message was not delivered!');
      }
      else {
        $this->getLogger('conditional_notification')->notice('A issue informer message was successfully delivered!');
      }
    }
  }
}
