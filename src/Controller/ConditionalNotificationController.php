<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Controller;

use Drupal\conditional_notification\Helper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Returns responses for Conditional Notification routes.
 */
final class ConditionalNotificationController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    private readonly Helper $helperService,
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_type.manager'),
      $container->get('conditional_notification.helper'),
    );
  }

  /**
   * Builds the response.
   */
  public function defaultTemplates(): array {

    // $configs = \Drupal::entityTypeManager()->getStorage('conditional_notification')->loadMultiple([]);

    $route_params = \Drupal::routeMatch()->getParameters();


    foreach ($route_params as $param_key => $param_value) {
      if ($param_value instanceof ConfigEntityInterface) {
        $entity_type = $param_value->getEntityType()->getBundleOf();
        $entity_bundle = $param_value->id();
      }
    }

    $destination = \Drupal::destination()->getAsArray();

    if (isset($entity_type) && isset($entity_bundle)) {
      $url = Url::fromRoute('entity.cn_template.add_form', ['entity_type' => $entity_type, 'bundle' => $entity_bundle],['query' => $destination]);
      $build['links'] = [
        '#title' => $this->t('Add notification template'),
        '#type' => 'link',
        '#url' => $url,
        '#attributes' => ['class' => ['button', 'btn']]
      ];
    }

    $build['content'] = \Drupal::entityTypeManager()->getListBuilder('cn_template')->render();

    return $build;


  }

  public function entityTemplates(): array {

    $destination = \Drupal::destination()->getAsArray();

    $supported = $this->helperService->getEnabledSupportedEntityBundles();

    foreach ($supported as $support) {
      $supported_entity = $this->helperService->getEntityTypeBundleFromString($support);      
      if ($entity = \Drupal::routeMatch()->getParameter($supported_entity['type'])) {
        
        if (!$entity instanceof EntityInterface) {
          $entity = $this->entityTypeManager->getStorage($supported_entity['type'])->load($entity);
        }       
        
        $override_default_notification_url = Url::fromRoute('conditional_notification.override_default_notification_template', ['entity_type' => $entity->getEntityTypeId(), 'bundle' => $entity->bundle(), 'entity_id' => $entity->id()],['query' => $destination]);
        
        /*

        $url = Url::fromRoute('entity.conditional_notification.add_form', ['entity_type' => $entity->getEntityTypeId(), 'bundle' => $entity->bundle()],['query' => $destination]);
        $build['links']['add_notification_template'] = [
          '#title' => $this->t('Add notification template'),
          '#type' => 'link',
          '#url' => $url,
          '#attributes' => ['class' => ['button', 'btn']]
        ];

        */

        $build['links']['override_default_notification_template'] = [
          '#title' => $this->t('Override default notification template'),
          '#type' => 'link',
          '#url' => $override_default_notification_url,
          '#attributes' => [
            'class' => ['button', 'btn', 'use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => json_encode(['height' => 600, 'width' => 800]),
          ]
        ];

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';




      }
    }

    $build['content'] = \Drupal::entityTypeManager()->getListBuilder('cn_template')->render();

    if (isset($build['content']['table']['#empty'])) {
      $build['content']['table']['#empty'] = $this->t('No conditional notifications exists yet!');
    }

    return $build;    
    

  }

  public function defaultTemplateAccess(AccountInterface $account) {

    $route_params = \Drupal::routeMatch()->getParameters();


    foreach ($route_params as $param_key => $param_value) {
      if ($param_value instanceof ConfigEntityInterface) {
        $entity_type = $param_value->getEntityType()->getBundleOf();
        $entity_bundle = $param_value->id();
      }
    }
   
    $supported = $this->helperService->getEnabledSupportedEntityBundles();

    foreach ($supported as $support) {
      $entity = $this->helperService->getEntityTypeBundleFromString($support);      
      if ($entity_type === $entity['type'] && $entity_bundle === $entity['bundle']) {
        return AccessResult::allowedIf(
          $account->hasPermission('administer conditional_notification') ||
          $account->hasPermission('manage conditional_notification defaults')
        );
      }
    }

    return AccessResult::forbidden();  

  }  

  public function entityTemplateAccess(AccountInterface $account) {

    $route_params = \Drupal::routeMatch()->getParameters(); 


 
    $supported = $this->helperService->getEnabledSupportedEntityBundles();

    foreach ($supported as $support) {
      $entity = $this->helperService->getEntityTypeBundleFromString($support);      
      if (\Drupal::routeMatch()->getParameter($entity['type'])) {
        return AccessResult::allowedIf(
          $account->hasPermission('administer conditional_notification') ||
          $account->hasPermission('manage conditional_notification overrides')        
        );
      }
    }

    return AccessResult::allowed();
    


  }  


}
