<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\conditional_notification\CnTemplateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\conditional_notification\Plugin\ECA\Event\ConditionalNotificationConditionUpdateTriggerEvent;
use Drupal\conditional_notification\Plugin\ECA\Event\ConditionalNotificationConditionCronTriggerEvent;
use Drupal\conditional_notification\Plugin\ECA\Event\ConditionalNotificationConditionInsertTriggerEvent;
use Drupal\conditional_notification\Plugin\ECA\Event\ConditionalNotificationRecipientsEvent;    


/**
 * @todo Add class description.
 */
final class Helper {

  use StringTranslationTrait;

  use LoggerChannelTrait;

  /**
   * Constructs a Helper object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly AccountProxyInterface $currentUser,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly EntityFieldManagerInterface $entityFieldManager,
  ) {}

  /**
   * @todo Add method description.
   */
  public function getAvailableFormats(): array {
    
    $filter_options = [];
    $current_user = $this->currentUser;
    $current_user_account = \Drupal::entityTypeManager()->getStorage('user')->load($current_user->id());

    $available_filters = filter_formats($current_user_account);    
  
    foreach($available_filters as $id => $filter) {
      $filter_options[$id] = $filter->label();
    }   
    
    return $filter_options;

  }

  public function getContentEntityTypes(): array {

    $result = [];

    $entity_types = $this->entityTypeManager->getDefinitions();

    $result = [];
    foreach ($entity_types as $name => $entity_type) {
      if ($entity_type instanceof ContentEntityType) {
        $result[$name] = [
          'label' => $entity_type->getLabel()->render(),
          'class' => $entity_type->getClass(),
          'provided by' => $entity_type->getProvider(),
        ];
      }
    }
      

    return $result;

  }

  public function getSupportedEntityBundles() {

    $supported_entity_bundles = [];

    $entity_types = $this->getContentEntityTypes();

    foreach ($entity_types as $name => $entity_type) {
      $entitiy_bundle = $this->entityTypeBundleInfo->getBundleInfo($name);
      foreach ($entitiy_bundle as $bundle => $bundle_info) {
        $entity_bundles_key = $name . ':' . $bundle;
        if ($bundle_info['label'] instanceof TranslatableMarkup) {
          $entity_bundles_label = $bundle_info['label']->render();          
        }
        else {
          $entity_bundles_label = $bundle_info['label'];
        }
        $supported_entity_bundles[$entity_bundles_key] = [
          'label' => $entity_bundles_label,
          'entity_type' => $entity_type['label']        
        ];
      }
    }  
    
    return $supported_entity_bundles; 

    

  }

  public function getECAConditionModels($filter_action_type = NULL) {

    $eca_models = ['_none' => ''];

    $entities = $this->entityTypeManager->getStorage('eca')
      ->loadMultiple();

    if (!empty($entities)) {
      foreach ($entities as $eca) {
        foreach ($eca->getUsedEvents() as $eca_event_id => $ecaEvent) {          
          $eca_id = $eca->id();
          $plugin = $ecaEvent->getPlugin();
          if ($plugin instanceof ConditionalNotificationConditionUpdateTriggerEvent || $plugin instanceof ConditionalNotificationConditionInsertTriggerEvent || $plugin instanceof ConditionalNotificationConditionCronTriggerEvent) {
            if (isset($filter_action_type)) {
              if ($filter_action_type === 'update') {
                if ($plugin instanceof ConditionalNotificationConditionUpdateTriggerEvent) {
                  $eca_models[$eca_id] = $eca->label();
                }
              }
              elseif ($filter_action_type === 'insert') {
                if ($plugin instanceof ConditionalNotificationConditionInsertTriggerEvent) {
                  $eca_models[$eca_id] = $eca->label();
                }
              }
              elseif ($filter_action_type === 'cron') {
                if ($plugin instanceof ConditionalNotificationConditionCronTriggerEvent) {
                  $eca_models[$eca_id] = $eca->label();
                }
              }
            }
            else {            
              $eca_models[$eca_id] = $eca->label();
            }
          }
        }
      }
    }

    return $eca_models;

  }

  public function getECAContextModels() {

    $eca_models = ['_none' => ''];

    $entities = $this->entityTypeManager->getStorage('eca')
      ->loadMultiple();

    if (!empty($entities)) {
      foreach ($entities as $eca) {
        foreach ($eca->getUsedEvents() as $eca_event_id => $ecaEvent) {          
          $eca_id = $eca->id();
          $plugin = $ecaEvent->getPlugin();
          if ($plugin instanceof ConditionalNotificationRecipientsEvent) {
            $eca_models[$eca_id] = $eca->label();
          }
        }
      }
    }

    return $eca_models;

  }  

  public function getEntityTypeBundleFromString(string $string) {

    $entity_type_bundle_array = explode(":", $string);

    $entity_type_bundle = [
      'type' => $entity_type_bundle_array[0],
      'bundle' => $entity_type_bundle_array[1]
    ];

    return $entity_type_bundle;

  }

  public function getEnabledSupportedEntityBundles() {

    $supported_entity_bundles = [];

    $config = $this->configFactory->get('conditional_notification.settings');

    if ($config) {
      $entity_type_supported_bundles = $config->get('supported_entities');  
      if (isset($entity_type_supported_bundles) && !empty($entity_type_supported_bundles)) { 
        foreach ($entity_type_supported_bundles as $key => $value) {
          if ($key === $value) {
            $supported_entity_bundles[$key] = $key;
          }
        }
      }

    }

    return $supported_entity_bundles;


  }

  public function getEnabledSupportedEntityType() {



  }

  public function isEntityTypeSupportedByEntityTypeId($entity_type_id) {

    $supported_entity_bundles = [];
   
    $config = $this->configFactory->get('conditional_notification.settings');

    if ($config) {
      $entity_type_supported_bundles = $config->get('supported_entities');  
      
      if (isset($entity_type_supported_bundles) && !empty($entity_type_supported_bundles)) { 
        foreach ($entity_type_supported_bundles as $key => $value) {
          if ($key === $value) {
            if ($entity_type_bundle = $this->getEntityTypeBundleFromString($key)) {
              $supported_entity_bundles[$key] = $entity_type_bundle['type'];
            }            
          }
        }
      }

      if (in_array($entity_type_id, $supported_entity_bundles)) {
        return TRUE;
      }

    }

    return FALSE;


  }

  public function isEntityTypeSupported($entity_type_bundle_string) {

    $supported_entity_bundles = [];
   
    $config = $this->configFactory->get('conditional_notification.settings');

    if ($config) {
      $entity_type_supported_bundles = $config->get('supported_entities');  
      
      if (isset($entity_type_supported_bundles) && !empty($entity_type_supported_bundles)) { 
        foreach ($entity_type_supported_bundles as $key => $value) {
          if ($key === $value) {
            $supported_entity_bundles[$key] = $entity_type_bundle['type'];                      
          }
        }
      }

      if (in_array($entity_type_bundle_string, $supported_entity_bundles)) {
        return TRUE;
      }

    }

    return FALSE;


  }   

  public function getSupportedDateFields($entity_type_id, $entity_bundle) {

    $supported_fields = ['_none' => ''];

    if (isset($entity_type_id) && isset($entity_bundle)) {
      // Check on that bundle
      $fields = $this->entityFieldManager
        ->getFieldDefinitions($entity_type_id, 
                            $entity_bundle);

      $base_fields = $this->entityFieldManager
        ->getBaseFieldDefinitions($entity_type_id, 
          $entity_bundle);

      foreach ($base_fields as $key => $field) {
        if (in_array($field->getType(), ['created','changed'])) {
          $machine_name_and_type = $key . ':' . $field->getType();  
          $supported_fields[$machine_name_and_type] = $field->getName();
        }
      }          
          
      foreach ($fields as $key => $field) {
        if ($field->getType() === 'datetime' || $field->getType() === 'smartdate') {
          $machine_name_and_type = $key . ':' . $field->getType();  
          $supported_fields[$machine_name_and_type] = $field->getName();
        }
      }




    }

    return $supported_fields;

  }  

  public function getSupportedDateParts(string $machine_name_and_type): array {

    $date_parts = [];

    $machine_name_and_type_array = explode(':', $machine_name_and_type);
    if (isset($machine_name_and_type_array) && is_array($machine_name_and_type_array) && !empty($machine_name_and_type_array)) {
      if (isset($machine_name_and_type_array[1])) {
        $type = $machine_name_and_type_array[1];
        if ($type === 'smartdate') {
          $date_parts['value'] = $this->t('Start');
          $date_parts['end_value'] = $this->t('End');
        }
      } 
    }

    return $date_parts;

  }

  public function createDefaultTemplatesForOverride($data, $entity_id, $entity_type, $entity_bundle) {

    $storage = $this->entityTypeManager->getStorage('cn_template');
    $default_templates = $storage->loadMultiple($data);

    if (!empty($default_templates)) {
      foreach ($default_templates as $id => $default_template) {    
        $new_id = $id . '_' . $entity_type . '_' . $entity_bundle . '_' . $entity_id;
        $new_template = $storage->create();
        $new_template->setEntityId($entity_id);
        $new_template->setNotificationId($id);
        $new_template->set('label', $default_template->label());
        $new_template->setSubject($default_template->getSubject());
        $new_template->setBody(['value' => $default_template->getBody(), 'format' => $default_template->getBodyFormat()]);
        $new_template->setNotificationEntityType($default_template->getNotificationEntityType());
        $new_template->setNotificationEntityBundle($default_template->getNotificationEntityBundle());
        $new_template->setEntityActionType($default_template->getEntityActionType());
        $new_template->setNotificationConditionEca($default_template->getNotificationConditionEca());
        $new_template->setNotificationContextEca($default_template->getNotificationContextEca());
        $new_template->setNotificationType('override');
        $new_template->setTimeOffset($default_template->getTimeOffset());
        $new_template->setTimeOffsetType($default_template->getTimeOffsetType());
        if ($default_template->getTimeOffsetDateField()) {
          $new_template->setTimeOffsetDateField($default_template->getTimeOffsetDateField());
        }       
        $new_template->save();  
      }
    }
  }

  public function mailReplyToAvailable(CnTemplateInterface $conditional_notification) {
    $mail_reply_to = FALSE;
    if (!empty($conditional_notification->getMailReplyTo())) {
      $mail_reply_to = $conditional_notification->getMailReplyTo();
    } 
    return $mail_reply_to;
  }

  public function getConditionalNotificationsWhenTimeOffset() {

    $data = [];

    $conditions = [
      'time_offset_enabled' => TRUE
    ];

    $conditional_notifications = $this->entityTypeManager->getStorage('cn_template')->loadByProperties($conditions);
    if (!empty($conditional_notifications)) {
      foreach ($conditional_notifications as $machine_name => $notification) {
        $data['machine_name'] = [
          'entity_type' => $notification->getNotificationEntityType(),
          'entity_bundle' => $notification->getNotificationEntityBundle(),
          'date' => [
            'field' => $notification->getTimeOffsetDateField(),
            'part' => $notification->getTimeOffsetDateFieldPart()
          ],
          'time_offset' => $notification->getTimeOffset(),
          'time_offset_type' => $notification->getTimeOffsetType(),
        ];
      }
    }

    return $data;
  
  }

  /**
   * Set notification logs to unprocessed.
   *
   * @param EntityInterface $entity
   * @return void
   */
  public function setConditionalNotificationLogToUnprocessed(EntityInterface $entity) {    
    $query = \Drupal::entityQuery('conditional_notification_log')
      ->condition('entity_id', $entity->id())
      ->condition('entity_type', $entity->getEntityTypeId())
      ->accessCheck(FALSE);
  
    $results = $query->execute();

    if (!empty($results)) {
      $logs = $this->entityTypeManager->getStorage('conditional_notification_log')->loadMultiple($results);
      foreach ($logs as $log) {
        $log->processed->value = 0;
        $log->save();
      }
    }
  }

  public function getSupportedEntityTypes($show_bundle = FALSE) {
    $config = $this->configFactory->get('conditional_notification.settings');
    $supported_entities = $config->get('supported_entities');
    $supported_entity_types = [];

    foreach ($supported_entities as $key => $value) {
      if ($key === $value) {
        $entity_type_bundle = explode(':', $key);
        $entity_type = $entity_type_bundle[0];
        $bundle = $entity_type_bundle[1];
        if ($show_bundle) {
          $supported_entity_types[$bundle] = $bundle;
        }
        else {
          $supported_entity_types[$entity_type] = $entity_type;
        }
      }
    }

    return $supported_entity_types;

    //$entity_type_bundle = explode(':', $supported_entity_types);

    //return $entity_type_bundle;



  }

}


