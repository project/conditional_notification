<?php

namespace Drupal\conditional_notification\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Notification context plugin item annotation object.
 *
 * @see \Drupal\conditional_notification\Plugin\NotificationContextManager
 * @see plugin_api
 *
 * @Annotation
 */
class NotificationContext extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The array with entities for which this plugin is allowed.
   *
   * @var array
   */
  public $entities;  

}
