<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Entity;

use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the conditional notification entity type.
 *
 * @ConfigEntityType(
 *   id = "conditional_notification",
 *   label = @Translation("Conditional Notification"),
 *   label_collection = @Translation("Conditional Notifications"),
 *   label_singular = @Translation("conditional notification"),
 *   label_plural = @Translation("conditional notifications"),
 *   label_count = @PluralTranslation(
 *     singular = "@count conditional notification",
 *     plural = "@count conditional notifications",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\conditional_notification\ConditionalNotificationListBuilder",
 *     "form" = {
 *       "add" = "Drupal\conditional_notification\Form\ConditionalNotificationForm",
 *       "edit" = "Drupal\conditional_notification\Form\ConditionalNotificationForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "conditional_notification",
 *   admin_permission = "administer conditional_notification",
 *   links = {
 *     "collection" = "/admin/structure/conditional-notification",
 *     "add-form" = "/admin/structure/conditional-notification/add",
 *     "edit-form" = "/admin/structure/conditional-notification/{conditional_notification}",
 *     "delete-form" = "/admin/structure/conditional-notification/{conditional_notification}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "subject",
 *     "body",
 *     "mail_from",
 *     "mail_reply_to",
 *     "mail_from_enabled",
 *     "notification_entity_type",
 *     "notification_entity_bundle",
 *     "entity_action_type",
 *     "notification_context",
 *     "notification_condition",
 *     "notification_id",
 *     "entity_id",
 *     "notification_type",
 *     "time_offset_enabled",
 *     "time_offset",
 *     "time_offset_type",
 *     "time_offset_date_field",
 *     "time_offset_date_field_part",
 *     "notification_context_eca",
 *     "notification_condition_eca",
 *  },
 * )
 */
final class ConditionalNotification extends ConfigEntityBase implements ConditionalNotificationInterface {

  /**
   * The ID of the notification.
   */
  protected string $id;

  /**
   * The notification label.
   */
  protected string $label;

  /**
   * The notification subject.
   */
  protected string $subject = '';

  /**
   * The notification body.
   */
  protected array $body = [];

  /**
   * The mail from mail.
   */
  protected string $mail_from = '';  

  /**
   * The mail reply to.
   */
  protected string $mail_reply_to = '';   

  /**
   * The mail from mail.
   */
  protected $mail_from_enabled = FALSE;    

  /**
   * The notification entity type.
   */
  protected string $notification_entity_type = '';

  /**
   * The notification entity bundle.
   */
  protected string $notification_entity_bundle = '';  

  /**
   * The entity action that triggers a notification.
   */
  protected string $entity_action_type = 'insert';

  /**
   * The notification context.
   */
  protected string $notification_context = '';

  /**
   * The notification condition.
   */
  protected string $notification_condition = '';

  /**
   * The notification reference to a default notification.
   */
  protected string $notification_id = '';  

  /**
   * The reference to an entity.
   */  
  protected string $entity_id = '';

  /**
   * The notification type 'default' or 'override'.
   */    
  protected string $notification_type = 'default';

  /**
   * The time offset enabled toggle.
   */ 
  protected $time_offset_enabled = FALSE;

  /**
   * The time offset for date fields.
   */    
  protected int $time_offset = 0;

  /**
   * The time offset type for date fields.
   */    
  protected string $time_offset_type = 'days';  

  /**
   * The time offset date field.
   */   
  protected string $time_offset_date_field = '';

  /**
   * The time offset date field part.
   */   
  protected string $time_offset_date_field_part = '';

  /**
   * The condition eca model machine name.
   */
  protected string $notification_condition_eca = '';

  /** 
   * The context eca model machine name.
   */
  protected string $notification_context_eca = '';    

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $id) {
    $this->id = $id;
    return $this;
  }    

  
  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubject(string $subject) {
    $this->subject = $subject;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * {@inheritdoc}
   */
  public function setBody(array $body) {
    $this->body = $body;
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getMailFrom() {
    return $this->mail_from;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailFrom(string $mail_from) {
    $this->mail_from = $mail_from;
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public function getMailReplyTo() {
    return $this->mail_reply_to;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailReplyTo(string $mail_reply_to) {
    $this->mail_reply_to = $mail_reply_to;
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getMailFromEnabled() {
    return $this->mail_from_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailFromEnabled($mail_from_enabled) {
    $this->mail_from_enabled = (bool) $mail_from_enabled;
    return $this;
  }     

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityType() {
    return $this->notification_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityType(string $notification_entity_type) {
    $this->notification_entity_type = $notification_entity_type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityBundle() {
    return $this->notification_entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityBundle(string $notification_entity_bundle) {
    $this->notification_entity_bundle = $notification_entity_bundle;
    return $this;
  }  

  public function getNotificationEntityTypeBundleInfo() {

    $entity_type_bundle_info = '';

    if (
      !empty($this->getNotificationEntityBundle()) && 
      !empty($this->getNotificationEntityType())
    ) {
      $entity_type = $this->getNotificationEntityType();
      $entity_bundle = $this->getNotificationEntityBundle();
      $entity_type_bundle_info = $entity_type . ':' . $entity_bundle;
    }

    return $entity_type_bundle_info;

  }

  public function getSupportedDateFields() {

    $helper_service = \Drupal::service('conditional_notification.helper');

    $entity_type_id = $this->getNotificationEntityType();
    $entity_bundle = $this->getNotificationEntityBundle();

    return $helper_service->getSupportedDateFields($entity_type_id, $entity_bundle);  

  }

  /**
   * {@inheritdoc}
   */
  public function getEntityActionType() {
    return $this->entity_action_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityActionType(string $entity_action_type) {
    $this->entity_action_type = $entity_action_type;
    return $this;
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getNotificationContext() {
    return $this->notification_context;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationContext(string $notification_context) {
    $this->notification_context = $notification_context;
    return $this;
  }   
  
  /**
   * {@inheritdoc}
   */
  public function getNotificationCondition() {
    return $this->notification_condition;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationCondition(string $notification_condition) {
    $this->notification_condition = $notification_condition;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationId() {
    return $this->notification_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationId(string $notification_id) {
    $this->notification_id = $notification_id;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getEntityId() {
    return $this->entity_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityId(string $entity_id) {
    $this->entity_id = $entity_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationType() {
    return $this->notification_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationType(string $notification_type) {
    $this->notification_type = $notification_type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetEnabled() {
    return $this->time_offset_enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetEnabled($time_offset_enabled) {
    $this->time_offset_enabled = (bool) $time_offset_enabled;
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public function getTimeOffset() {
    return $this->time_offset;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffset(int $time_offset) {
    $this->time_offset = $time_offset;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetType() {
    return $this->time_offset_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetType(string $time_offset_type) {
    $this->time_offset_type = $time_offset_type;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateField() {
    return $this->time_offset_date_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldOnly() {
    return strstr($this->time_offset_date_field, ":", true);
  }  

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateField(string $time_offset_date_field) {
    $this->time_offset_date_field = $time_offset_date_field;
    return $this;
  }    


  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldPart() {
    return $this->time_offset_date_field_part;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateFieldPart(string $time_offset_date_field_part) {
    $this->time_offset_date_field_part = $time_offset_date_field_part;
    return $this;
  }     

  /**
   * {@inheritdoc}
   */
  public function getNotificationConditionEca() {
    return $this->notification_condition_eca;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationConditionEca(string $notification_condition_eca) {
    $this->notification_condition_eca = $notification_condition_eca;
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getNotificationContextEca() {
    return $this->notification_context_eca;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationContextEca(string $notification_context_eca) {
    $this->notification_context_eca = $notification_context_eca;
    return $this;
  }     

 /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $notification_entity_type = \Drupal::request()->get('entity_type');

    $notification_entity_bundle = \Drupal::request()->get('bundle');

    if (!$this->getNotificationEntityType()) {
      $this->setNotificationEntityType($notification_entity_type);
    }

    if (!$this->getNotificationEntityBundle()) {
      $this->setNotificationEntityBundle($notification_entity_bundle);
    }

  }



}
