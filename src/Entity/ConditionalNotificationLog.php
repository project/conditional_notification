<?php

namespace Drupal\conditional_notification\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\conditional_notification\ConditionalNotificationLogInterface;

/**
 * Provides the 'conditional_notification_log' Entity.
 *
 * @package Drupal\conditional_notification\Entity
 *
 * @ContentEntityType(
 *   id = "conditional_notification_log",
 *   label = @Translation("Notification log"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData"
 *   },
 *   base_table = "conditional_notification_log",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class ConditionalNotificationLog extends ContentEntityBase implements ConditionalNotificationLogInterface {

  use EntityChangedTrait;


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['notification_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notification ID'))
      ->setSetting('target_type', 'conditional_notification')
      ->setSetting('handler', 'default')
      ->setDescription(t('The notification ID.'));
      
    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The Entity ID.'));
    
    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setDescription(t('The Entity type.'));
      
    $fields['time_offset_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Time offset type'))
      ->setDescription(t('The time offset type.'));     
      
    $fields['time_offset'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Time offset'));    
      
    $fields['entity_date_field'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity date field'));   
      
    $fields['entity_date_field_part'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity date field part'));
      
    $fields['entity_date_field_timestamp'] = BaseFieldDefinition::create('integer')
      ->setSettings([
        'unsigned' => true,
        'size' => 'big'
      ])
      ->setLabel(t('Entity date field timestamp')); 
      
    $fields['entity_date_offset'] = BaseFieldDefinition::create('integer')
      ->setSettings([
        'unsigned' => true,
        'size' => 'big'
      ])
      ->setLabel(t('Entity date offset'));     
      
    $fields['entity_date_string_day'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity date string day'));          
     
    $fields['entity_date_string_hour'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity date string hour'));    
      
    $fields['processed'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Processed'));    
      
    $fields['processed_timestamp'] = BaseFieldDefinition::create('integer')
      ->setSettings([
        'unsigned' => true,
        'size' => 'big'
      ])
      ->setLabel(t('Processed on'));

    return $fields;
    
  }


}
