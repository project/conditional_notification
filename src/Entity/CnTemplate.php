<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Entity;

use Drupal\conditional_notification\CnTemplateInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the conditional notification store entity class.
 *
 * @ContentEntityType(
 *   id = "cn_template",
 *   label = @Translation("Conditional Notification Template"),
 *   label_collection = @Translation("Conditional Notification Templates"),
 *   label_singular = @Translation("conditional notification template"),
 *   label_plural = @Translation("conditional notification templates"),
 *   label_count = @PluralTranslation(
 *     singular = "@count conditional notification templates",
 *     plural = "@count conditional notification templates",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\conditional_notification\CnTemplateListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\conditional_notification\CnTemplateAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\conditional_notification\Form\CnTemplateForm",
 *       "edit" = "Drupal\conditional_notification\Form\CnTemplateForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" = \Drupal\Core\Entity\Form\RevisionDeleteForm::class,
 *       "revision-revert" = \Drupal\Core\Entity\Form\RevisionRevertForm::class,
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\conditional_notification\Routing\CnTemplateHtmlRouteProvider",
 *       "revision" = \Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider::class,
 *     },
 *   },
 *   base_table = "cn_template",
 *   data_table = "cn_template_field_data",
 *   revision_table = "cn_template_revision",
 *   revision_data_table = "cn_template_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer conditional notification template",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/conditional-notification-template",
 *     "add-form" = "/admin/structure/conditional-notification-template/add",
 *     "canonical" = "/admin/structure/conditional-notification-template/{cn_template}",
 *     "edit-form" = "/admin/structure/conditional-notification-template/{cn_template}",
 *     "delete-form" = "/admin/structure/conditional-notification-template/{cn_template}/delete",
 *     "delete-multiple-form" = "/admin/content/conditional-notification-template/delete-multiple",
 *     "revision" = "/admin/structure/conditional-notification-template/{cn_template}/revision/{cn_template_revision}/view",
 *     "revision-delete-form" = "/admin/structure/conditional-notification-template/{cn_template}/revision/{cn_template_revision}/delete",
 *     "revision-revert-form" = "/admin/structure/conditional-notification-template/{cn_template}/revision/{cn_template_revision}/revert",
 *     "version-history" = "/admin/structure/conditional-notification-template/{cn_template}/revisions",
 *   },
 *   field_ui_base_route = "entity.cn_template.settings",
 * )
 */
final class CnTemplate extends RevisionableContentEntityBase implements CnTemplateInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    $notification_entity_type = \Drupal::request()->get('entity_type');
    $notification_entity_bundle = \Drupal::request()->get('bundle');

    if (isset($notification_entity_type)) {
      if (!$this->getNotificationEntityType()) {
        $this->setNotificationEntityType($notification_entity_type);
      }
    }

    if (isset($notification_entity_bundle)) {
      if (!$this->getNotificationEntityBundle()) {
        $this->setNotificationEntityBundle($notification_entity_bundle);
      }
    }

    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    // The label for the notification
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // The status of the notification
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    // The description of the notification
    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // The subject of the notification
    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subject'))
      ->setDescription(t('The subject of the Notification.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])        
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The body of the notification
    $fields['body'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Body'))
      ->setDescription(t('The body of the notification.'))
      ->setSettings([
        'display_summary' => FALSE, // Set to TRUE if you want a summary field.
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea_with_summary',
        'weight' => 1,
        'settings' => [
          'rows' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ]);    

    // mail_from_enabled
    $fields['mail_from_enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Mail from enabled'))
      ->setDescription(t('A boolean indicating whether the mail from address should be used.'))
      ->setDefaultValue(FALSE)
      ->setSettings(['on_label' => 'On', 'off_label' => 'Off'])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'boolean',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mail_from'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mail from'))
      ->setDescription(t('The mail from address of the Notification.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The Mail reply to
    $fields['mail_reply_to'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mail reply to'))
      ->setDescription(t('The mail reply to address of the Notification.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // notification_entity_type - Hidden from form, just for inernal use
    $fields['notification_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notification entity type'))
      ->setDescription(t('The name of the type for which we want to deliver a notification.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ]);
    
    // notification_entity_bundle - Hidden from form, just for internal use
    $fields['notification_entity_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notification entity bundle'))
      ->setDescription(t('The name of the bundle for which we want to deliver a notification.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ]);
      
    // entity_action_type - Hiden from form, just for internal use
    $fields['entity_action_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notification action type'))
      ->setDescription(t('The name of the action for which we want to deliver a notification.'))
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ]);
      
    // Notification ID - Hidden from form, just for internal use
    $fields['notification_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('The notification ID'))
      ->setDescription(t('The notification id from the store.'))
      ->setSetting('target_type', 'cn_template');

      /*
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', FALSE);
      */


      
    // Entity ID - Hidden from form, just for internal use
    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('An integer field.'));

    // time_offset_enabled
    $fields['time_offset_enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable Time offset'))
      ->setDescription(t('A boolean indicating whether the mail from address should be used.'))
      ->setDefaultValue(FALSE)
      ->setSettings(['on_label' => 'On', 'off_label' => 'Off'])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'boolean',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // time_offset
    $fields['time_offset'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Time offset'))
      ->setDescription(t('An integer field example.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // time_offset_type
    $fields['time_offset_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Time offset type'))
      ->setDescription(t('Choose days or hours.'))
      ->setDefaultValue(200)
      ->setSettings([
        'allowed_values' => [
          'days' => 'Days',
          'hours' => 'Hours',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // time_offset_date_field
    $fields['time_offset_date_field'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Time offset date field'))
      ->setDescription(t('The date field for the offset.'))
      ->setRequired(FALSE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    // time_offset_date_field_part
    $fields['time_offset_date_field_part'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Time offset date field part'))
      ->setDescription(t('The date field part for the offset in case of a smart date.'))
      ->setRequired(FALSE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');      

    // ECA notification_condition_eca
    $fields['notification_condition_eca'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Notification condition ECA'))
      ->setDescription(t('The ECA condition for the notification.'))
      ->setRequired(FALSE)
      ->setSettings([
        'allowed_values_function' => 'flexible_access_eca_condition_allowed_values',
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // ECA notification_context_eca
    $fields['notification_context_eca'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Notification context ECA'))
      ->setDescription(t('The ECA context for the notification.'))
      ->setRequired(FALSE)
      ->setSettings([
        'allowed_values_function' => 'flexible_access_eca_context_allowed_values',
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);



    // time_offset_date_field_part
    $fields['notification_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notification type'))
      ->setDescription(t('The notification type.'))
      ->setRequired(FALSE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('default');
    
    // User ID
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the conditional notification template was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the conditional notification template was last edited.'));

    return $fields;
    
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->get('subject')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubject(string $subject) {
    $this->set('subject', $subject);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->get('body')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBodyFormat() {
    return $this->get('body')->format;
  }  

  /**
   * {@inheritdoc}
   */
  public function setBody(array $body) {
    $this->set('body', $body);
    return $this;
  }   
  

  /**
   * {@inheritdoc}
   */
  public function getMailFrom() {
    return $this->get('mail_from')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailFrom(string $mail_from) {
    $this->set('mail_from', $mail_from);
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public function getMailReplyTo() {
    return $this->get('mail_reply_to')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailReplyTo(string $mail_reply_to) {
    $this->set('mail_reply_to', $mail_reply_to);
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getMailFromEnabled() {
    return $this->get('mail_from_enabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailFromEnabled($mail_from_enabled) {
    $this->set('mail_from_enabled', $mail_from_enabled);
    return $this;
  }     

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityType() {
    return $this->get('notification_entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityType(string $notification_entity_type) {
    $this->set('notification_entity_type', $notification_entity_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationEntityBundle() {
    return $this->get('notification_entity_bundle')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationEntityBundle(string $notification_entity_bundle) {
    $this->set('notification_entity_bundle', $notification_entity_bundle);
    return $this;
  }  

  public function getNotificationEntityTypeBundleInfo() {

    $entity_type_bundle_info = '';

    if (
      !empty($this->getNotificationEntityBundle()) && 
      !empty($this->getNotificationEntityType())
    ) {
      $entity_type = $this->getNotificationEntityType();
      $entity_bundle = $this->getNotificationEntityBundle();
      $entity_type_bundle_info = $entity_type . ':' . $entity_bundle;
    }

    return $entity_type_bundle_info;

  }

  public function getSupportedDateFields() {

    $helper_service = \Drupal::service('conditional_notification.helper');

    $entity_type_id = $this->getNotificationEntityType();
    $entity_bundle = $this->getNotificationEntityBundle();

    return $helper_service->getSupportedDateFields($entity_type_id, $entity_bundle);  

  }

  /**
   * {@inheritdoc}
   */
  public function getEntityActionType() {
    return $this->get('entity_action_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityActionType(string $entity_action_type) {
    $this->set('entity_action_type', $entity_action_type);
    return $this;
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getNotificationId() {
    return $this->get('notification_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationId($notification_id) {
    $this->set('notification_id', $notification_id);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityId(string $entity_id) {
    $this->set('entity_id', $entity_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationType() {
    return $this->get('notification_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationType(string $notification_type) {
    $this->set('notification_type', $notification_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetEnabled() {
    return $this->get('time_offset_enabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetEnabled($time_offset_enabled) {
    $this->set('time_offset_enabled', $time_offset_enabled);
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public function getTimeOffset() {
    return $this->get('time_offset')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffset($time_offset) {
    $this->set('time_offset', intval($time_offset));
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetType() {
    return $this->get('time_offset_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetType(string $time_offset_type) {
    $this->set('time_offset_type', $time_offset_type);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateField() {
    return $this->get('time_offset_date_field')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldOnly() {
    return strstr($this->get('time_offset_date_field')->value, ":", true);
  }  

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateField($time_offset_date_field) {
    $this->set('time_offset_date_field', $time_offset_date_field);
    return $this;
  }    


  /**
   * {@inheritdoc}
   */
  public function getTimeOffsetDateFieldPart() {
    return $this->get('time_offset_date_field_part')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTimeOffsetDateFieldPart($time_offset_date_field_part) {
    $this->set('time_offset_date_field_part', $time_offset_date_field_part);
    return $this;
  }     

  /**
   * {@inheritdoc}
   */
  public function getNotificationConditionEca() {
    return $this->get('notification_condition_eca')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationConditionEca(string $notification_condition_eca) {
    $this->set('notification_condition_eca', $notification_condition_eca);
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getNotificationContextEca() {
    return $this->get('notification_context_eca')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotificationContextEca(string $notification_context_eca) {
    $this->set('notification_context_eca', $notification_context_eca);
    return $this;
  }

  public function getStatus() {
    return $this->get('status')->value;
  }

  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }



}
