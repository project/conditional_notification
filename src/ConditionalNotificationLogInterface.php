<?php

namespace Drupal\conditional_notification;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Conditional Notification Log entity.
 *
 * @ingroup conditional_notification
 * @package Drupal\conditional_notification
 */
interface ConditionalNotificationLogInterface extends ContentEntityInterface, EntityChangedInterface {

}
