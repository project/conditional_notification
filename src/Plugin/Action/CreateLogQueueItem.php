<?php

namespace Drupal\conditional_notification\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\conditional_notification\Event\ConditionCronTriggerEvent;
use Drupal\conditional_notification\Event\ConditionInsertTriggerEvent;
use Drupal\conditional_notification\Event\ConditionUpdateTriggerEvent;
use Drupal\conditional_notification\Event\RecipientsEvent;

/**
 * Set the result output of a performed model.
 *
 * @Action(
 *   id = "conditional_notification_create_log_queue_item",
 *   label = @Translation("Conditional Notification: Create log queue item for action type of CRON"),
  *  description = @Translation("Creates a queue for the performed model. This action only works upon <em>conditional_notification</em> events."),
 *   eca_version_introduced = "1.0.0",
 *   type = "system"
 * )
 */
class CreateLogQueueItem extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return ['module' => ['eca', 'conditional_notification']];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'result' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access_result = ($this->event instanceof ConditionCronTriggerEvent || $this->event instanceof ConditionInsertTriggerEvent || $this->event instanceof ConditionUpdateTriggerEvent || $this->event instanceof RecipientsEvent) ? AccessResult::allowed() : AccessResult::forbidden();
    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $event = $this->event;
    if (!($event instanceof ConditionCronTriggerEvent)) {
      return;
    }

    $data = $event->data;

    if ($data['time_offset_enabled'] && $data['entity_date_offset']) {
      \Drupal::queue('conditional_notification_notification_log_queue')->createItem($data);
    }
    else {
      $debug = json_encode($data);
      \Drupal::logger('conditional_notification')
        ->warning('Problem creating log. Debug Info: @debug.', [
          '@debug' => $debug	
      ]);
    }

  }

}
