<?php

namespace Drupal\conditional_notification\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\conditional_notification\Event\RecipientsEvent;

/**
 * Set the result output of a performed model.
 *
 * @Action(
 *   id = "recipient_set_result",
 *   label = @Translation("Recipients: Set result as array"),
  *  description = @Translation("Set the result output of a performed model as array. This action only works upon <em>conditional_notification</em> events."),
 *   eca_version_introduced = "1.0.0",
 *   type = "system"
 * )
 */
class RecipientSetResult extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return ['module' => ['eca', 'conditional_notification']];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'result' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['result'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Result'),
      '#default_value' => $this->configuration['result'],
      '#weight' => 10,
      '#eca_token_replacement' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['result'] = $form_state->getValue('result');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access_result = ($this->event instanceof RecipientsEvent) ? AccessResult::allowed() : AccessResult::forbidden();

    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging: ACCESS RESULT <pre><code>' . print_r($access_result, TRUE) . '</code></pre>');

    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {


    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging');
    

    $event = $this->event;
    if (!($event instanceof RecipientsEvent)) {
      return;
    } 
    
    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging after event');
    

    $result = $this->tokenService->replaceClear($this->configuration['result']);


    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging: <pre><code>' . print_r($result, TRUE) . '</code></pre>');
    

    $result_as_array = explode(',', $result);

    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging: <pre><code>' . print_r($result_as_array, TRUE) . '</code></pre>');
    


    if (!empty($result_as_array) && is_array($result_as_array)) {
      $result_as_array_harmonized = array_combine($result_as_array, $result_as_array);      
    }
    else {
      $result_as_array_harmonized = [];
    }

    $event->result = $result_as_array_harmonized; 
    
    \Drupal::logger('conditional_notification')->debug('Conditional notification recipieint set result debugging: <pre><code>' . print_r($result_as_array_harmonized, TRUE) . '</code></pre>');
    
  }

}
