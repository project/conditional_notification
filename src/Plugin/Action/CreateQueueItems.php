<?php

namespace Drupal\conditional_notification\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\conditional_notification\Event\ConditionCronTriggerEvent;
use Drupal\conditional_notification\Event\ConditionInsertTriggerEvent;
use Drupal\conditional_notification\Event\ConditionUpdateTriggerEvent;
use Drupal\conditional_notification\Event\RecipientsEvent;

/**
 * Set the result output of a performed model.
 *
 * @Action(
 *   id = "conditional_notification_create_queue_items",
 *   label = @Translation("Conditional Notification: Create queue item"),
  *  description = @Translation("Set the result output of a performed model. This action only works upon <em>conditional_notification</em> events."),
 *   eca_version_introduced = "1.0.0",
 *   type = "system"
 * )
 */
class CreateQueueItems extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return ['module' => ['eca', 'conditional_notification']];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'result' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['result'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Result'),
      '#default_value' => $this->configuration['result'],
      '#weight' => 10,
      '#eca_token_replacement' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['result'] = $form_state->getValue('result');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access_result = ($this->event instanceof ConditionCronTriggerEvent || $this->event instanceof ConditionInsertTriggerEvent || $this->event instanceof ConditionUpdateTriggerEvent || $this->event instanceof RecipientsEvent) ? AccessResult::allowed() : AccessResult::forbidden();
    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $event = $this->event;
    if (!($event instanceof ConditionCronTriggerEvent || $event instanceof ConditionInsertTriggerEvent || $event instanceof ConditionUpdateTriggerEvent || $this->event instanceof RecipientsEvent)) {
      return;
    }

    $data = [
      "entity_id" => $event->getEntity()->id(),
      "entity_type" => $event->entityType,
      "bundle" => $event->bundle,
      "notification_id" => $event->notification_id,
    ];

    $event_model_name = $event->ecaModelMachineName;

    $allowed = $this->configuration['result'];

    \Drupal::logger('conditional_notification')
      ->debug('Conditional Notification: Create queue items: @result : @notification_id : @event_model_name', ['@result' => $allowed, '@notification_id' => $event->notification_id, '@event_model_name' => $event_model_name]);

    if ($allowed) {
      \Drupal::queue('conditional_notification_notification_queue')->createItem($data);
    }  
    

  }

}
