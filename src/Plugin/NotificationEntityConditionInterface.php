<?php

namespace Drupal\conditional_notification\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Notification entity condition plugins.
 */
interface NotificationEntityConditionInterface extends PluginInspectionInterface {

  /**
   * Checks if this is a valid entity condition for the action.
   */
  public function isValidEntityCondition($entity);

}
