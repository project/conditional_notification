<?php

namespace Drupal\conditional_notification\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for Notification content plugins.
 */
interface NotificationContextInterface extends PluginInspectionInterface {

  /**
   * Returns a batched list of recipients for this context.
   *
   * @param array $data
   *   The data.
   *
   * @return array
   *   An associative array of recipients, containing the following key-value
   *   pairs:
   *   - user_id: The user ID.
   *   - user_id: The user ID.
   */
  public function getRecipients(array $data): array;

  /**
   * Determines if the entity is valid for this context.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return bool
   *   TRUE if it's valid entity.
   */
  public function isValidEntity(EntityInterface $entity): bool;

}
