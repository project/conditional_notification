<?php

namespace Drupal\conditional_notification\Plugin\NotificationContext;

use Drupal\conditional_notification\Plugin\NotificationContextBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\QueryFactory;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group\Entity\GroupRelationship;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a 'GroupMembersNotificationContext' notification context.
 *
 * @NotificationContext(
 *   id = "group_members_noticiation_context",
 *   label = @Translation("Group members"),
 *   entities = {"group"}
 * )
 */
class GroupMembersNotificationContext extends NotificationContextBase {


  /**
   * Constructs a MentionActivityContext object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\Query\Sql\QueryFactory $entity_query
   *   The query factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    QueryFactory $entity_query,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_query, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query.sql'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipients(array $data): array {
    $recipients = []; 
    
    // We only know the context if there is an entity_type and entity_id.
    if (
      isset($data['entity_type']) && !empty($data['entity_type']) &&
      isset($data['entity_id']) && !empty($data['entity_id'])
    ) {
      $group = $this->entityTypeManager->getStorage($data['entity_type'])->load($data['entity_id']);
      if ($group instanceof GroupInterface) {
        $members = $group->getMembers();
        if (!empty($members)) {
          foreach ($members as $member) {
            $user = $member->getUser();
            if ($user instanceof UserInterface && $user->getStatus() === TRUE ) {
              $recipients[$user->id()] = $user->id();
            }
          }
        }
      }
    }

    return $recipients;
    
  }

}
