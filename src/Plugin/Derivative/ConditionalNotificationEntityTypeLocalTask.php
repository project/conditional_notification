<?php

namespace Drupal\conditional_notification\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\conditional_notification\Helper;

/**
 * Provides local task definitions for all supported entity bundles.
 *
 * @see \Drupal\conditional_notification\Controller\EntityDebugController
 * @see \Drupal\conditional_notification\Routing\RouteSubscriber
 */
class ConditionalNotificationEntityTypeLocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;  

  /**
   * The helper service.
   *
   * @var \Drupal\conditional_notification\Helper
   */
  protected $helperService;   

  /**
   * Creates an DevelLocalTask object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\conditional_notification\Helper $helper_service
   *   The helper service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager, 
    TranslationInterface $string_translation, 
    ConfigFactoryInterface $config_factory,
    Helper $helper_service
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->config = $config_factory->get('conditional_notification.settings');
    $this->helperService = $helper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('string_translation'),
      $container->get('config.factory'),
      $container->get('conditional_notification.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $supported_entities = $this->config->get('supported_entities');

    if (empty($supported_entities)) {
      return $this->derivatives;
    }

    foreach ($supported_entities as $entity_type => $enabled) {
      if ($entity_type === $enabled) {
        $entity_type_bundle = $this->helperService->getEntityTypeBundleFromString($entity_type);
        $type = $entity_type_bundle['type'];
        $bundle = $entity_type_bundle['bundle'];

        if ($type && $bundle) {
          foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {

            if ($entity_type->getBundleOf() === $type) {

            $this->derivatives["$entity_type_id.conditional_notification_tab"] = [
              'route_name' => "entity.$entity_type_id." . 'conditional_notification_default_templates',
              'title' => $this->t('Conditional Notification'),
              'base_route' => "entity.$entity_type_id." . 'edit_form',
              'weight' => 100,
            ];

          }
          }        
        }
      }
    }      
   

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}