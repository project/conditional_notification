<?php

namespace Drupal\conditional_notification\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Utility\Html;

/**
 * Provides the Notification context plugin manager.
 */
class NotificationContextManager extends DefaultPluginManager {

  /**
   * Constructor for NotificationContextManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/NotificationContext', $namespaces, $module_handler, 'Drupal\conditional_notification\Plugin\NotificationContextInterface', 'Drupal\conditional_notification\Annotation\NotificationContext');

    $this->alterInfo('conditional_notification_notification_context_info');
    $this->setCacheBackend($cache_backend, 'conditional_notification_notification_context_plugins');
  }

  /**
   * Retrieves an options list of available trackers.
   *
   * @return string[]
   *   An associative array mapping the IDs of all available tracker plugins to
   *   their labels.
   */
  public function getOptionsList(array $entities = []) {
    $options = [];

    if (empty($entities)) {
      foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
        $options[$plugin_id] = Html::escape($plugin_definition['label']);
      }
    }
    else {
      foreach ($entities as $entity) {
        // Get all entity condition plugin definitions.
        foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
          if (in_array($entity, $plugin_definition['entities'])) {
            $options[$plugin_id] = Html::escape($plugin_definition['label']);
          }
        }
      }      
    }


    return $options;
  }

}
