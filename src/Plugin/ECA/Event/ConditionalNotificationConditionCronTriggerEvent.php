<?php

namespace Drupal\conditional_notification\Plugin\ECA\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Attributes\Token;
use Drupal\eca\Entity\Objects\EcaEvent;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\conditional_notification\Event\ConditionCronTriggerEvent;
use Symfony\Contracts\EventDispatcher\Event;
use Drupal\eca\Service\ContentEntityTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the ECA Events regarding Conditional Notification.
 *
 * @EcaEvent(
 *   id = "conditional_notification_condition_cron_trigger",
 *   deriver = "Drupal\conditional_notification\Plugin\ECA\Event\ConditionalNotificationConditionCronTriggerEventDeriver",
 *   documentation = @Translation("Triggered on CRON. Available parameters: entity, ENTITY_TYPE, BUNDLE, notification_type, eca_model_machine_name"), 
 *   eca_version_introduced = "1.0.0",
 * )
 */
class ConditionalNotificationConditionCronTriggerEvent extends EventBase {


  /**
   * The entity type service.
   *
   * @var \Drupal\eca\Service\ContentEntityTypes
   */
  protected ContentEntityTypes $entityTypes;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->entityTypes = $container->get('eca.service.content_entity_types');
    return $plugin;
  }    

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'conditional_notification_condition' => [
        'label' => 'Conditional notification condition fired on CRON',
        'event_name' => \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::EVENT_NAME,
        'event_class' => \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
        'tags' => Tag::RUNTIME | Tag::EPHEMERAL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      //'entity' => '',
      //'entity_type' => '',
      //'bundle' => '',
      //'notification_type' => '',
      //'eca_model_machine_name' => '', 
      'type' => ContentEntityTypes::ALL,
      'eca_model_machine_name_field' => '',         
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
  
    $form['eca_model_machine_name_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECA Model Machine Name'),
      '#default_value' => $this->configuration['eca_model_machine_name'] ?? '',
      '#description' => $this->t('Define the ECA model machine name.'),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type (and bundle)'),
      '#options' => $this->entityTypes->getTypesAndBundles(FALSE),
      '#default_value' => $this->configuration['type'],
    ];   
  
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['eca_model_machine_name_field'] = $form_state->getValue('eca_model_machine_name_field');
    $this->configuration['type'] = $form_state->getValue('type');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'event',
    description: 'The event.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],
    properties: [
      new Token(name: 'entity', description: 'The entity.'),
      new Token(name: 'ENTITY_TYPE', description: 'The entity type of the entity.'),
      new Token(name: 'BUNDLE', description: 'The bundle of the entity.'),
      new Token(name: 'notification_type', description: 'Notification type of the noctification.'),
      new Token(name: 'eca_model_machine_name', description: 'The eca model machine name.'),
      new Token(name: 'eca_model_machine_name_field', description: 'The eca model machine name field.'),
    ],
  )]
  protected function buildEventData(): array {
    $event = $this->event;
    $data = [];
    if ($event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      $data += [
        'entity' => $event->getEntity(),
        'entity_type' => $event->entityType,
        'bundle' => $event->bundle,
        'notification_type' => $event->notificationType,
        'notification_id' => $event->notification_id,
        'data' => $event->data,
        'eca_model_machine_name' => $event->ecaModelMachineName,   
        'eca_model_machine_name_field' => $this->configuration['eca_model_machine_name_field'],    
        'type' => $this->configuration['type'],     
      ];
    }
    $data += parent::buildEventData();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'entity',
    description: 'The entity.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],
  )]
  #[Token(
    name: 'ENTITY_TYPE',
    description: 'ENTITY TYPE of the entity.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],
  )]
  #[Token(
    name: 'BUNDLE',
    description: 'BUNDLE of the entity.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],
  )]  
  #[Token(
    name: 'notification_type',
    description: 'Notification type.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],
  )]
  #[Token(
    name: 'eca_model_machine_name',
    description: 'The eca model machine name.',
    classes: [
      \Drupal\conditional_notification\Event\ConditionCronTriggerEvent::class,
    ],       
  )]
  #[Token(
    name: 'eca_model_machine_name_field',
    description: 'The eca model machine name field.',
    classes: [
      \Drupal\conditional_notification\Event\RecipientsEvent::class,
    ],       
  )]  
  #[Token(
    name: 'type',
    description: 'The entity type.',
    classes: [
      \Drupal\conditional_notification\Event\RecipientsEvent::class,
    ],       
  )]     
  public function getData(string $key): mixed {
    $event = $this->event;
    if ($key === 'entity' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      return $event->getEntity();
    }
    if ($key === 'entity_type' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      return $event->entityType;
    }
    if ($key === 'bundle' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      return $event->bundle;
    }    
    if ($key === 'notification_type' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      return $event->notificationType;
    }  
    if ($key === 'notification_id' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {      
      return $event->notification_id;
    }   
    if ($key === 'data' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {      
      return $event->data;
    }     
    if ($key === 'eca_model_machine_name' && $event instanceof \Drupal\conditional_notification\Event\ConditionCronTriggerEvent) {
      return $event->ecaModelMachineName;
    }   
    if ($key === 'eca_model_machine_name_field' && $event instanceof \Drupal\conditional_notification\Event\RecipientsEvent) {
      return $this->configuration['eca_model_machine_name_field'];
    }       
    if ($key === 'type' && $event instanceof \Drupal\conditional_notification\Event\RecipientsEvent) {
      return $this->configuration['type'];
    }     
    
    return parent::getData($key);
  }
  
 

}
