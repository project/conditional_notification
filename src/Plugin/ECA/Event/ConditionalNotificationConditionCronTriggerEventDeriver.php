<?php

namespace Drupal\conditional_notification\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for ECA Conditional Notification event plugins.
 */
class ConditionalNotificationConditionCronTriggerEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return ConditionalNotificationConditionCronTriggerEvent::definitions();
  }

}
