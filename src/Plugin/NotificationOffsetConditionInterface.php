<?php

namespace Drupal\conditional_notification\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Notification offset condition plugins.
 */
interface NotificationOffsetConditionInterface extends PluginInspectionInterface {

  /**
   * Checks if this is a valid offset condition for the entity.
   */
  public function isValidOffsetCondition($entity);

}
