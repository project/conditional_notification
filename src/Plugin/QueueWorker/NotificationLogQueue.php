<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Plugin\QueueWorker;

use Drupal\conditional_notification\Helper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Render\PlainTextOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Render\Markup;

/**
 * Defines 'conditional_notification_notification_log_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "conditional_notification_notification_log_queue",
 *   title = @Translation("Notification LOG Queue"),
 *   cron = {"time" = 60},
 * )
 */
final class NotificationLogQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Constructs a new NotificationQueue instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly MailManagerInterface $mailManager,
    private readonly LanguageManagerInterface $languageManager,
    private readonly EmailValidatorInterface $emailValidator,
    private readonly Token $token,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly AccountProxyInterface $currentUser,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Helper $conditionalNotificationHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('email.validator'),
      $container->get('token'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('conditional_notification.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
   
    // Get the notification id.
    $notification_id = $data['notification_id'];
    // Get the entity type.
    $entity_type = $data['entity_type'];
    // Get the entity id.
    $entity_id = $data['entity_id'];
    // Get the time offset type
    $time_offset_type = $data['time_offset_type'];
    // Get the time offset
    $time_offset = $data['time_offset'];
    // Get entity date field
    $entity_date_field = $data['entity_date_field'];
    // Get entity date field part
    $entity_date_field_part = $data['entity_date_field_part'];
    // Get entity_date_field_timestamp
    $entity_date_field_timestamp = $data['entity_date_field_timestamp'];
    // Get the entity date offset
    $entity_date_offset = $data['entity_date_offset'];
    // Get the hour as string
    $entity_date_string_hour = $data['entity_date_string_hour'];
    // Get the day as string
    $entity_date_string_day = $data['entity_date_string_day'];


    if ($this->checkIfRecordsAlreadyExists($data)) {
      // Update record
      $this->updateRecord($data);
    }
    else {
      try {
        $storage = $this->entityTypeManager->getStorage('conditional_notification_log');
        $storage->create([
          'entity_id' => $entity_id,
          'entity_type' => $entity_type,
          'notification_id' => $notification_id,
          'time_offset_type' => $time_offset_type,
          'time_offset' => $time_offset,
          'entity_date_field' => $entity_date_field,
          'entity_date_field_part' => $entity_date_field_part,
          'entity_date_field_timestamp' =>  intval($entity_date_field_timestamp),
          'entity_date_offset' => intval($entity_date_offset),
          'entity_date_string_day' => $entity_date_string_day,
          'entity_date_string_hour' => $entity_date_string_hour, 
          'processed' => 0,         
        ])->save();          
      }
      catch (\Exception $exception) {
        $message = $exception->getMessage();
        $this->getLogger('conditional_notification')->warning('Problem writing conditional notification log with field: @field',['@field' => $notification_id]);
        $this->getLogger('conditional_notification')->warning('Problem writing conditional notification log with field: @field',['@field' => $entity_id]);
        $this->getLogger('conditional_notification')->warning('Problem writing conditional notification log with field: @field',['@field' => $entity_date_field_timestamp]);
        $this->getLogger('conditional_notification')->warning('Problem writing conditional notification log with field entity date offset: @field',['@field' => $entity_date_offset]);
        $this->getLogger('conditional_notification')->warning('Problem writing conditional notification log: @message',['@message' => $message]);
      }
    }
  }

  protected function checkIfRecordsAlreadyExists($data) {

    $record_exists = FALSE;

    $query = \Drupal::entityQuery('conditional_notification_log')
      ->condition('notification_id', $data['notification_id'])
      ->condition('entity_id', $data['entity_id'])
      ->condition('entity_type', $data['entity_type'])
      ->accessCheck(FALSE);

    $results = $query->execute();
    
    if (!empty($results)) {
      $record_exists = TRUE;
    }

    return $record_exists;

  }

  protected function updateRecord($data) {

    // Some data we do not need.
    unset($data['time_offset_enabled']);    

    $conditions = [
      'notification_id' => $data['notification_id'],
      'entity_id' => $data['entity_id'],
      'entity_type' => $data['entity_type'],
    ];

    try {

      $logs = $this->entityTypeManager->getStorage('conditional_notification_log')->loadByProperties($conditions);
      if ($log = reset($logs)) {
        if ($log instanceof EntityInterface) {
          foreach ($data as $field => $value) {
            if ($field === 'notification_id') {
              $log->{$field}->target_id = $value;  
            }
            else {
              $log->{$field}->value = $value;
            }          
          }
          $log->save();
        }
      }
    }
    catch (\Exception $exception) {
      $message = $exception->getMessage();
      $this->getLogger('conditional_notification')->warning('Problem updating conditional notification log: @message',['@message' => $message]);
    }
  }

}
