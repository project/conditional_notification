<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Plugin\QueueWorker;

use Drupal\conditional_notification\Helper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\conditional_notification\CnTemplateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\Token;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Render\PlainTextOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Render\Markup;
use Drupal\conditional_notification\Event\RecipientsEvent;
use Drupal\conditional_notification\Event\ConditionCronTriggerEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines 'conditional_notification_notification_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "conditional_notification_notification_queue",
 *   title = @Translation("Notification Queue"),
 *   cron = {"time" = 60},
 * )
 */
final class NotificationQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Constructs a new NotificationQueue instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly MailManagerInterface $mailManager,
    private readonly LanguageManagerInterface $languageManager,
    private readonly EmailValidatorInterface $emailValidator,
    private readonly Token $token,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly AccountProxyInterface $currentUser,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Helper $helper,
    private readonly EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('email.validator'),
      $container->get('token'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('conditional_notification.helper'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {

    if (!isset($data['notification_id']) && !isset($data['entity_type']) && !isset($data['entity_id'])) {
      return;
    }
  
    // Get the notification id.
    $notification_id = $data['notification_id'];
    // Get the entity type.
    $entity_type = $data['entity_type'];
    // Get the entity id.
    $entity_id = $data['entity_id'];
    // Get entity.
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    // Define entity data
    $entity_data = [
      'entity_type' => $entity_type,
      'entity' => $entity
    ];

    $notification = $this->entityTypeManager->getStorage('cn_template')->load($notification_id);
    if ($notification instanceof CnTemplateInterface) {

      if (
        $notification->getNotificationEntityType() === $entity->getEntityTypeId() &&
        $notification->getNotificationEntityBundle() === $entity->bundle()          
      ) {

        // Get the context and get the recipients
        // Check if new eca conditions work
        $recipients_eca_model_machine_name = $notification->getNotificationContextEca();
        $event = new RecipientsEvent($entity, $data['entity_type'], $entity->bundle(), 'default', $recipients_eca_model_machine_name);
        $this->eventDispatcher->dispatch($event, RecipientsEvent::EVENT_NAME);  
        $recipients = $event->result;
        if (!empty($recipients) && is_array($recipients)) {
          $users = $this->entityTypeManager->getStorage('user')->loadMultiple($recipients);
          if (!empty($users)) {
            foreach ($users as $user) {
              $preferred_language = $user->getPreferredLangcode();
              $user_language = $this->languageManager->getLanguage($preferred_language);          
              $notification_content = $this->getNotificationContent($notification_id, $user_language);
              $this->sendMail($user->getEmail(), $preferred_language, $notification_content, $user, $entity_data);
            }
          }
          else {
            \Drupal::service('conditional_notification.issue_informer')->sendIssueInformerMail($data);
            $this->getLogger('conditional_notification')->warning('No users found to send notifications to!');
          }
        }
        else {
          \Drupal::service('conditional_notification.issue_informer')->sendIssueInformerMail($data);
          $debug = json_encode($data);
          $this->getLogger('conditional_notification')
               ->warning('No recipients found. Check the following plugin: @context. Debug info: @debug', [
                '@context' => $recipients_eca_model_machine_name, '@debug' => $debug
          ]);          
        }
      }
    }
  }

  /**
   * Send the email.
   *
   * @param string $user_mail
   *   The recipient email address.
   * @param string $langcode
   *   The recipient language.
   * @param \Drupal\conditional_notification\CnTemplateInterface $conditional_notification
   *   The subject and body field from the GroupWelcomeMessage Entity
   * @param string $display_name
   *   In case of anonymous users a display name will be given.
   */
  protected function sendMail(string $user_mail, string $langcode, CnTemplateInterface $conditional_notification, $user, $entity_data) {

    // Send Emails from the configured site mail
    $site_mail = \Drupal::config('system.site')->get('mail');

    // Get the entity type from entity data.
    $entity_type = $entity_data['entity_type'];

    // Get the entity from entity data.
    $entity = $entity_data['entity'];

    //$token_service = \Drupal::token();
    $token_context = [
      'user' => $user,
      $entity_type => $entity      
    ];

    $subject =  PlainTextOutput::renderFromHtml($this->token->replace($conditional_notification->getSubject(), $token_context));
    $body = $this->token->replace($conditional_notification->getBody(), $token_context);

    // Load user.module from the user module.
    \Drupal::service('module_handler')->loadInclude('user', 'module');


    $special_token_context = ['user' => $user];

    $special_token_options = [
      'langcode' => $langcode,
      'callback' => 'user_mail_tokens',
      'clear' => TRUE,
    ];

    $subject_special_tokens = PlainTextOutput::renderFromHtml($this->token->replace($subject, $special_token_context, $special_token_options));
    $body_special_tokens = $this->token->replace($body, $special_token_context, $special_token_options);
    
    $context = [
      'subject' => $subject_special_tokens,
      'message' => Markup::create($body_special_tokens),
    ];

    // Ensure html
    $context['params'] = array('format' => 'text/html');

    // Add reply to address if configured
    if ($mail_reply_to = $this->helper->mailReplyToAvailable($conditional_notification)) {
      $reply = $mail_reply_to;
    }
    else {
      $reply = NULL;
    }

    // Sending Email
    $delivered = $this->mailManager->mail('system', 'action_send_email', $user_mail, $langcode, [
      'context' => $context
    ], $reply);

    // Prepare logging data.
    $logging_data = [
      'recipient' => $user_mail,
      'entity_type' => $entity_type,
      'entity_id' => $entity->id(),
      'notification_id' => $conditional_notification->id(),
    ];    

    if(!$delivered) {
      \Drupal::moduleHandler()->invokeAll('conditional_notification_delivery_failed', [$logging_data]);    
    }
    else {
      \Drupal::moduleHandler()->invokeAll('conditional_notification_delivery_successful', [$logging_data]);
    }
  }

  protected function getNotificationContent($notification_id, LanguageInterface $language) {

    $content = FALSE;

    $notification = $this->entityTypeManager->getStorage('cn_template')->load($notification_id);

    if ($notification instanceof CnTemplateInterface) {
      $content = $notification;
    }

    return $content;

  }  

}
