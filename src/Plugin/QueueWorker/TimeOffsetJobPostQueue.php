<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Plugin\QueueWorker;

use Drupal\conditional_notification\Helper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Render\PlainTextOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;


/**
 * Defines 'conditional_notification_time_offset_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "conditional_notification_time_offset_job_post_queue",
 *   title = @Translation("Time offset job post queue"),
 *   cron = {"time" = 60},
 * )
 */
final class TimeOffsetJobPostQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Constructs a new NotificationQueue instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly LanguageManagerInterface $languageManager,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly AccountProxyInterface $currentUser,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Helper $conditionalNotificationHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('conditional_notification.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($entity_data): void {
    $entity = $this->entityTypeManager->getStorage($entity_data['entity_type'])->load($entity_data['entity_id']);
    if ($entity instanceof EntityInterface) {
      \Drupal::service('conditional_notification.notification_factory')->createNotifications($entity); 
    }    
  }

}

