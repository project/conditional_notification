<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\conditional_notification\Helper;
use Drupal\conditional_notification\IssueInformer;

/**
 * Configure Conditional Notification settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The helper service.
   *
   * @var \Drupal\conditional_notification\Helper
   */
  protected $helperService;  

  /**
   * The Issue informer service.
   *
   * @var \Drupal\conditional_notification\IssueInformer
   */
  protected $issueInformer;    

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\conditional_notification\Helper $helper_service
   *   The helper service.
   * @param \Drupal\conditional_notification\IssueInformer $issue_informer
   *   The issue informer service. 
   */
  public function __construct(ConfigFactoryInterface $config_factory, Helper $helper_service, IssueInformer $issue_informer) {
    parent::__construct($config_factory);
    $this->helperService = $helper_service;
    $this->issueInformer = $issue_informer;
  }  

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('conditional_notification.helper'),
      $container->get('conditional_notification.issue_informer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'conditional_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['conditional_notification.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $entity_types = $this->helperService->getSupportedEntityBundles();

    //\Drupal::moduleHandler()->alter('conditinal_notification_entitiy_types', $plugin_items);

    $supported_entities_options = $entity_types;

    $supported_entities_default = $this->config('conditional_notification.settings')->get('supported_entities') ?? [];

    $header = array(
      'label' => t('Entity Bundle'),
      'entity_type' => t('Entity Type')
    );

    $form['supported_entities'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $supported_entities_options,
      '#empty' => $this->t('No content entities available.'),
      '#default_value' => $supported_entities_default,      
    ];

    $text_format_options = $this->helperService->getAvailableFormats();
    $notification_text_format_default = $this->config('conditional_notification.settings')->get('notification_text_format') ?? []; 

    $form['notification_text_format'] = [
      '#title' => $this->t('Enable the supported text format for notifications'),
      '#type' => 'select',
      '#options' => $text_format_options,
      '#default_value' => $notification_text_format_default,
    ];

    $issue_informer_emails_default = $this->config('conditional_notification.settings')->get('issue_informer_emails') ?? '';

    $form['issue_informer_emails'] = [
      '#title' => $this->t('Notification informer emails'),
      '#description' => $this->t('Get a notification when a conditional notification has not been sent. Use ; to separate the emails.'),
      '#type' => 'textarea',
      '#default_value' => $issue_informer_emails_default,
    ];

    $issue_informer_subject_default = $this->config('conditional_notification.settings')->get('issue_informer_subject') ?? '';

    $form['issue_informer_subject'] = [
      '#title' => $this->t('Notification informer subject'),
      '#description' => $this->t('Define your unique subject for those kind of emails.'),
      '#type' => 'textfield',
      '#default_value' => $issue_informer_subject_default,
    ];    

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!empty($form_state->getValue('issue_informer_emails'))) {
      $issue_informer_emails = trim($form_state->getValue('issue_informer_emails'));
      $emails = explode(';', $issue_informer_emails);
      foreach ($emails as $email) {
        if (!$this->issueInformer->isEmailValid($email)) {
          $form_state->setErrorByName(
            'issue_informer_emails',
            $this->t('Unvalid emails detected! Did you use ; as delimiter to separate the email addresses?'),
          );
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('conditional_notification.settings')
      ->set('supported_entities', $form_state->getValue('supported_entities'))
      ->set('notification_text_format', $form_state->getValue('notification_text_format'))
      ->set('issue_informer_emails', $form_state->getValue('issue_informer_emails'))
      ->set('issue_informer_subject', $form_state->getValue('issue_informer_subject'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
