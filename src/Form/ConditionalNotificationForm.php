<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\conditional_notification\Entity\ConditionalNotification;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Conditional Notification form.
 */
final class ConditionalNotificationForm extends EntityForm {

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create(
      $container
    );    
    $instance->setCurrentUser($container->get('current_user'));
    return $instance;
  }   

  /**
   * Sets current user.
   */
  public function setCurrentUser(AccountInterface $account) {
    $this->currentUser = $account;
  }  

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $settings = \Drupal::config('conditional_notification.settings');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [ConditionalNotification::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#description' => $this->t('You can make use of tokens for the subject field. Ex: [user:display-name].'),
      '#default_value' => $this->entity->getSubject() ?? '',
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#default_value' => $this->entity->getBody()['value'] ?? '',
      //'#required' => TRUE,
      '#format' => $settings->get('notification_text_format'),
      //'#allowed_formats' => [
        //$settings->get('selected_format')
       //]  
    ]; 

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    if ($this->entity->isNew()) {
      $hide_fields = [
        'subject',
        'body',
        'notification_condition',
      ];

      foreach ($hide_fields as $field) {
        $form[$field]['#attributes']['class'][] = 'hidden';
        $form[$field]['#title_display'] = 'invisible';
        $form[$field]['#description_display'] = 'invisible';        
      }   
    }
    else {

      $available_tokens = [
        $this->entity->getNotificationEntityType(),
        'user',
      ];     


      $form['tokens'] = \Drupal::service('token.tree_builder')->buildRenderable($available_tokens);


      if ($this->entity->getEntityId()) {
        $entity_action_type_access = FALSE;
      }
      else {
        $entity_action_type_access = TRUE;
      }

      // Define options
      $entity_action_type_options = [
        'insert' => $this->t('Insert'),
        'update' => $this->t('Update'),
        'cron' => $this->t('Cron'),
      ];

      $entity_action_type_default = $this->entity->getEntityActionType() ?? 'update';

      $form['entity_action_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity Action type'),
        '#default_value' => $entity_action_type_default,
        '#description' => $this->t('Select "insert" or "update" as your action type.'),
        '#options' => $entity_action_type_options,
        '#access' => $entity_action_type_access,
        '#ajax' => [
          'callback' => '::getSupportedEntityConditionsDropDownCallback',
          'wrapper' => 'ajax-entity-conditions-wrapper',
        ],
      ];    

      $form['entity_condition_wrapper'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'ajax-entity-conditions-wrapper'],
      ]; 


      if (empty($form_state->getValue('entity_action_type'))) {
        $selected_entity_type_action = key($entity_action_type_options);
      }
      else {
        $selected_entity_type_action = $form_state->getValue('entity_action_type');
      }  
      
      $entity_condition_options = \Drupal::service('plugin.manager.notification_entity_condition.processor');
      $entity_type_bundle_info = $this->entity->getNotificationEntityTypeBundleInfo();  

      if ($saved_action_type = $this->entity->getEntityActionType()) {
        $entity_condition_options = $entity_condition_options->getOptionsList([$entity_type_bundle_info], [$saved_action_type]);
      }
      else {
        $entity_condition_options = $entity_condition_options->getOptionsList([$entity_type_bundle_info], [$selected_entity_type_action]); 
      }

               
      if ($this->currentUser->hasPermission('manage conditional_notification condition')) {
        $notification_condition_disabled = FALSE;
      } 
      else {
        $notification_condition_disabled = TRUE;
      }

      $form['entity_condition_wrapper']['notification_condition'] = [
        '#type' => 'select',
        '#title' => $this->t('Condition'),
        '#description' => $this->t('Condition that trigger sending the notification.'),
        '#default_value' => $this->entity->getNotificationCondition(),
        '#options' => $entity_condition_options,
        '#disabled' => $notification_condition_disabled,
      ];    
      
      if ($entity_type_id = $this->entity->getNotificationEntityType()) {
        $entity_type_for_context = $entity_type_id;
      }
      else {
        $entity_type_for_context = [];
      }


      $entity_type_for_context = $this->entity->getNotificationEntityType();  

      $notification_context_manager = \Drupal::service('plugin.manager.notification_context.processor');
      $context_options = $notification_context_manager->getOptionsList([$entity_type_for_context]);
      
      if ($this->currentUser->hasPermission('manage conditional_notification context')) {
        $notification_context_disabled = FALSE;
      } 
      else {
        $notification_context_disabled = TRUE;
      }

      $form['notification_context'] = [
        '#type' => 'select',
        '#title' => $this->t('Context'),
        '#description' => $this->t('Select the group of recipients of that noticication.'),
        '#default_value' => $this->entity->getNotificationContext(),
        '#options' => $context_options,
        '#disabled' => $notification_context_disabled
      ]; 

      if ($this->currentUser->hasPermission('manage conditional_notification mail from')) {
        $notification_mail_from_disabled = FALSE;
      } 
      else {
        $notification_mail_from_disabled = TRUE;
      }

      $form['mail_from_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Define a custom from email instead of site mail.'),
        '#default_value' => $this->entity->getMailFromEnabled(),
        '#disabled' => $notification_mail_from_disabled
      ];

      $form['mail_from'] = [
        '#type' => 'email',
        '#title' => $this->t('Mail form address'),
        '#default_value' => $this->entity->getMailFrom(),  
        '#disabled' => $notification_mail_from_disabled,
        '#states' => [
          'visible' => [
            ':input[name="mail_from_enabled"]' => ['checked' => TRUE],
          ],
        ]  
      ];

      if ($this->currentUser->hasPermission('manage conditional_notification mail reply to')) {
        $notification_mail_reply_to_disabled = FALSE;
      } 
      else {
        $notification_mail_reply_to_disabled = TRUE;
      }        

      $form['mail_reply_to'] = [
        '#type' => 'email',
        '#title' => $this->t('Mail reply to'),
        '#default_value' => $this->entity->getMailReplyTo(),  
        '#disabled' => $notification_mail_reply_to_disabled,
      ];    

      $condition_eca_update_options = \Drupal::service('conditional_notification.helper')->getECAConditionModels();

      $form['notification_condition_eca'] = [
        '#type' => 'select',
        '#title' => $this->t('Notification Condition ECA Model'),
        '#description' => $this->t('Select the ECA model that should be used for the condition.'),
        '#default_value' => $this->entity->getNotificationConditionEca() ?? '',
        '#options' => $condition_eca_update_options,
      ];

      $context_eca_options = \Drupal::service('conditional_notification.helper')->getECAContextModels();

      $form['notification_context_eca'] = [
        '#type' => 'select',
        '#title' => $this->t('Notification Context ECA Model'),
        '#description' => $this->t('Select the ECA model that should be used for the context.'),
        '#default_value' => $this->entity->getNotificationContextEca() ?? '',
        '#options' => $context_eca_options,
      ];      
      
      if (!empty($this->entity->getSupportedDateFields())) { 

        $supported_date_fields = $this->entity->getSupportedDateFields();

        if (empty($form_state->getValue('time_offset_date_field'))) {
          $selected_date_field = key($supported_date_fields);
        }
        else {
          $selected_date_field = $form_state->getValue('time_offset_date_field');
        }        


        if ($this->currentUser->hasPermission('manage conditional_notification time offset')) {
          $notification_time_offset_disabled = FALSE;
        } 
        else {
          $notification_time_offset_disabled = TRUE;
        }        

        $form['time_offset_enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable time offset'),
          '#default_value' => $this->entity->getTimeOffsetEnabled(),
          '#disabled' => $notification_time_offset_disabled,
          '#states' => [
            'visible' => [
              'select[name="entity_action_type"]' => ['value' => 'cron'],
            ],
          ]
        ];        

        $form['time_offset_wrapper'] = [
          '#type' => 'container',
          '#attributes' => ['id' => 'ajax-time-offset-wrapper'],
        ];   
        
        $form['time_offset_wrapper']['time_offset_date_field'] = [
          '#type' => 'select',
          '#title' => $this->t('Supported date fields'),
          '#description' => $this->t('Select one field you want to use the offset for.'),
          '#default_value' => $this->entity->getTimeOffsetDateField() ?? $selected_date_field,
          '#options' => $supported_date_fields,
          '#disabled' => $notification_time_offset_disabled,
          '#ajax' => [
            'callback' => '::getDatePartsDropDownCallback',
            'wrapper' => 'ajax-time-offset-wrapper',
          ],
          '#states' => [
            'visible' => [
              ':input[name="time_offset_enabled"]' => ['checked' => TRUE],
            ],
          ]
        ]; 
 
        $helper_service = \Drupal::service('conditional_notification.helper');

        if ($saved_date_field = $this->entity->getTimeOffsetDateField()) {
          $time_offset_date_field_part_options = $helper_service->getSupportedDateParts($saved_date_field);
        }
        else {
          $time_offset_date_field_part_options = $helper_service->getSupportedDateParts($selected_date_field);  
        }

        if (!empty($time_offset_date_field_part_options)) {
          if (empty($form_state->getValue('time_offset_date_field_part'))) {
            // Check if default value
            if ($date_field_part = $this->entity->getTimeOffsetDateFieldPart()) {
              $selected_date_field_part = $date_field_part;
            }
            else {
              $selected_date_field_part = key($time_offset_date_field_part_options);
            }            
          }
          else {
            if ($date_field_part = $this->entity->getTimeOffsetDateFieldPart()) {
              $selected_date_field_part = $form_state->getValue('time_offset_date_field_part');
            }
            else {
              $selected_date_field_part = '';
            }
          }
        }
        else {
          $selected_date_field_part = '';
        }
        
        $form['time_offset_wrapper']['time_offset_date_field_part'] = [
          '#type' => 'select',
          '#title' => $this->t('Date parts'),
          '#options' => $time_offset_date_field_part_options,
          '#disabled' => $notification_time_offset_disabled,
          '#default_value' => $selected_date_field_part,
          '#states' => [
            'visible' => [
              ':input[name="time_offset_enabled"]' => ['checked' => TRUE],
            ],
          ]          
        ];        




        
        

      }



    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  public function getDatePartsDropDownCallback(array $form, FormStateInterface $form_state) {
    return $form['time_offset_wrapper'];
  }

  public function getSupportedEntityConditionsDropDownCallback(array $form, FormStateInterface $form_state) {
    return $form['entity_condition_wrapper'];
  }  

}
