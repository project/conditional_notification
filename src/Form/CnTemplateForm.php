<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the conditional notification template entity edit forms.
 */
final class CnTemplateForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $time_offset_date_field_part_options = [];

    $helper_service = \Drupal::service('conditional_notification.helper');

    $notification_time_offset_disabled = TRUE;

    $entity_action_type_default = $this->entity->getEntityActionType() ?? 'update';

    // Define options
    $entity_action_type_options = [
      'insert' => $this->t('Insert'),
      'update' => $this->t('Update'),
      'cron' => $this->t('Cron'),
    ];   

    if ($this->entity->isNew()) {

      $form['custom_entity_action_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity Action type'),
        '#default_value' => $entity_action_type_default,
        '#description' => $this->t('Select "insert" or "update" as your action type.'),
        '#options' => $entity_action_type_options,
      ];
      foreach ($form as $key => $element) {
        if ($key !== 'label' && $key !== 'status') {
          if (isset($element['widget'])) {
            //$form[$key]['#access'] = FALSE;
            $form[$key]['#attributes']['class'][] = 'hidden';
            $form[$key]['#title_display'] = 'invisible';
            $form[$key]['#description_display'] = 'invisible';
          }
        }
      }

      // For now we override the entity_action_type because its only a string
      $entity_id = $this->entity->getEntityId();

      if (isset($entity_id) && $entity_id) {
        $entity_action_type_access = FALSE;
      }
      else {
        $entity_action_type_access = TRUE;
      }   
    }
    else {

      $filter_eca_conditions = \Drupal::service('conditional_notification.helper')->getECAConditionModels($entity_action_type_default);
      $form['notification_condition_eca']['widget']['#options'] = $filter_eca_conditions;


      if ($entity_action_type_default === 'cron') {
        $time_offset_active = TRUE;
      }
      else {
        $time_offset_active = FALSE;
      }

      $form['time_offset']['widget']['#access'] = $time_offset_active;
      $form['time_offset_enabled']['widget']['#access'] = $time_offset_active;
      $form['time_offset_type']['widget']['#access'] = $time_offset_active;

      if (!empty($this->entity->getSupportedDateFields()) && $time_offset_active) { 

        //dump($form);
  
        $supported_date_fields = $this->entity->getSupportedDateFields();
  
        if (empty($form_state->getValue('custom_time_offset_date_field'))) {
          $selected_date_field = key($supported_date_fields);
        }
        else {
          $selected_date_field = $form_state->getValue('custom_time_offset_date_field');
        }    


        


  
  
        if (\Drupal::currentUser()->hasPermission('manage conditional_notification time offset')) {
          $notification_time_offset_disabled = FALSE;
        } 
        else {
          $notification_time_offset_disabled = TRUE;
        }  

        $form['time_offset_enabled']['widget']['#disabled'] = $notification_time_offset_disabled;
        //$form['time_offset_enabled']['widget']['#default_value'] = $this->entity->get('time_offset_enabled')->value;  
        $form['time_offset_enabled']['widget']['#states'] = [
          'visible' => [
            'select[name="entity_action_type"]' => ['value' => 'cron'],
          ],
        ];       
        
        
        $form['time_offset_wrapper'] = [
          '#type' => 'container',
          '#attributes' => ['id' => 'ajax-time-offset-wrapper'],
          '#states' => [
            'visible' => [
              ':input[name="time_offset_enabled"]' => ['checked' => TRUE],
            ],
          ]
        ];  

        $form_time_offset_enabled = $form['time_offset_enabled'];
        unset($form['time_offset_enabled']);
        $form['time_offset_wrapper']['time_offset_enabled'] = $form_time_offset_enabled;


        $form_time_offset_type = $form['time_offset_type'];
        unset($form['time_offset_type']);  
        $form['time_offset_wrapper']['time_offset_type'] = $form_time_offset_type;

        $form_time_offset = $form['time_offset'];
        unset($form['time_offset']);
        $form['time_offset_wrapper']['time_offset'] = $form_time_offset;


        /*
        $form['time_offset_wrapper']['time_offset'] = [
          '#type' => 'number',
          '#title' => $this->t('Time offset'),
          '#description' => $this->t('Enter the time offset.'),
          '#default_value' => $this->entity->getTimeOffset() ?? 0,
          '#disabled' => $notification_time_offset_disabled,
        ];
        */
        
        
        
        $form['time_offset_wrapper']['custom_time_offset_date_field'] = [
          '#name' => 'custom_time_offset_date_field',
          '#type' => 'select',
          '#title' => $this->t('Supported date fields'),
          '#description' => $this->t('Select one field you want to use the offset for.'),
          '#default_value' => $this->entity->getTimeOffsetDateField() ?? $selected_date_field,
          '#options' => $supported_date_fields,
          '#disabled' => $notification_time_offset_disabled,
          '#ajax' => [
            'callback' => '::getDatePartsDropDownCallback',
            'wrapper' => 'ajax-time-offset-wrapper',
            'event' => 'change',
          ],
          '#weight' => 100,
        ]; 


        if ($saved_date_field = $this->entity->getTimeOffsetDateField()) {
          $time_offset_date_field_part_options = $helper_service->getSupportedDateParts($saved_date_field);
        }

        if (!empty($form_state->getValue('custom_time_offset_date_field'))) {
          $time_offset_date_field_part_options = $helper_service->getSupportedDateParts($selected_date_field); 
        }


        if (!empty($time_offset_date_field_part_options)) {
          if (empty($form_state->getValue('custom_time_offset_date_field_part'))) {
            // Check if default value
            if ($date_field_part = $this->entity->getTimeOffsetDateFieldPart()) {
              $selected_date_field_part = $date_field_part;
            }
            else {
              $selected_date_field_part = key($time_offset_date_field_part_options);
            }            
          }
          else {
            if ($date_field_part = $this->entity->getTimeOffsetDateFieldPart()) {
              $selected_date_field_part = $form_state->getValue('custom_time_offset_date_field_part');
            }
            else {
              $selected_date_field_part = '';
            }
          }
        }
        else {
          $selected_date_field_part = '';
        }     
      
        $form['time_offset_wrapper']['custom_time_offset_date_field_part'] = [
          '#type' => 'select',
          '#title' => $this->t('Date parts'),
          '#options' => $time_offset_date_field_part_options,
          '#disabled' => $notification_time_offset_disabled,
          '#default_value' => $selected_date_field_part,
          '#weight' => 101,
        ];   

        //$form['time_offset_wrapper']['#markup'] = $selected_date_field;
             


    
        $form['time_offset_wrapper']['time_offset']['widget']['#disabled'] = $notification_time_offset_disabled;


        $time_offset_type_options = [
          'days' => $this->t('Days'),
          'hours' => $this->t('Hours'),        
        ];
  
        //$form['time_offset_wrapper']['time_offset_type']['widget']['#default_value'] = $this->entity->getTimeOffsetType() ?? 'days';
        $form['time_offset_wrapper']['time_offset_type']['widget']['#disabled'] = $notification_time_offset_disabled;
        
        






       
      }
  





















    }




    //dump($form);

    //$test = \Drupal::service('conditional_notification.helper')->getSupportedEntityTypes();

    //$test2 = \Drupal::service('conditional_notification.helper')->getSupportedEntityBundles();

    //dump($test2);

    /*
    $form['notification_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Notification entity type'),
      '#options' => ['node' => 'Node', 'user' => 'User'],
      '#default_value' => $this->entity->get('notification_entity_type')->value,
      '#required' => TRUE,
    ];
    */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    
    if ($this->entity->isNew()) {
      $this->entity->set('entity_action_type', $form_state->getValue('custom_entity_action_type'));
    }
    $this->entity->set('time_offset_date_field', $form_state->getValue('custom_time_offset_date_field'));    
    $this->entity->set('time_offset_date_field_part', $form_state->getValue('custom_time_offset_date_field_part'));
    $this->entity->save();
    
    $result = parent::save($form, $form_state);



    //dump($form_state->getValues());
    //exit;

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New conditional notification template %label has been created.', $message_args));
        $this->logger('conditional_notification')->notice('New conditional notification template %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The conditional notification template %label has been updated.', $message_args));
        $this->logger('conditional_notification')->notice('The conditional notification template %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

  public function getDatePartsDropDownCallback(array $form, FormStateInterface $form_state) {
    return $form['time_offset_wrapper'];
  }

  public function getSupportedEntityConditionsDropDownCallback(array $form, FormStateInterface $form_state) {
    return $form['entity_condition_wrapper'];
  }    

}
