<?php

declare(strict_types=1);

namespace Drupal\conditional_notification\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\conditional_notification\Helper;

/**
 * Provides a Conditional Notification form.
 */
final class OverrideDefaultNotificationTemplate extends FormBase {

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * @var \Drupal\conditional_notification\Helper
   */
  protected $helperService;

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(AccountInterface $account, Helper $helper_service) {
    $this->account = $account;
    $this->helperService = $helper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('conditional_notification.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'conditional_notification_override_default_notification_template';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL, $bundle = NULL, $entity_id = NULL): array {

    $existing_default_templates = [];

    $header = [
      'id' => $this->t('Default notification template'),
    ];

    $entity_storage = \Drupal::entityTypeManager()->getStorage('cn_template');
    
    $entity_ids = $entity_storage->getQuery()
      ->condition('status', 1)
      ->condition('notification_entity_type', $entity_type)
      ->condition('notification_entity_bundle', $bundle)
      ->condition('notification_type', 'default')
      ->condition('entity_action_type', ['update','cron'], 'IN')
      ->accessCheck(FALSE)
      ->execute();

      \Drupal::logger('conditional_notification')->notice('Entity IDs: ' . print_r($entity_ids, TRUE));


    $overriden_entity_ids = $entity_storage->getQuery()
      ->condition('status', 1)
      ->condition('notification_entity_type', $entity_type)
      ->condition('notification_entity_bundle', $bundle)
      ->condition('notification_type', 'override')
      ->condition('entity_id', $entity_id, '=')  
      ->condition('entity_action_type', ['update','cron'], 'IN')
      ->accessCheck(FALSE)
      ->execute();

      \Drupal::logger('conditional_notification')->notice('overridden IDs: ' . print_r($overriden_entity_ids, TRUE));

    $entities = \Drupal::entityTypeManager()->getStorage('cn_template')->loadMultiple($overriden_entity_ids);

    if (isset($entities) && !empty($entities)) {
      foreach ($entities as $entity) {
        if (in_array($entity->getNotificationId(), $entity_ids)) {
          unset($entity_ids[$entity->getNotificationId()]);          
        }        
      }
    }

    $entities = \Drupal::entityTypeManager()->getStorage('cn_template')->loadMultiple($entity_ids);
    if (isset($entities) && !empty($entities)) {
      foreach ($entities as $entity) {
        $label = $entity->label() . ' (' . $entity->getEntityActionType() . ')';
        $existing_default_templates[$entity->id()] = ['id' => $label];
      }
    }

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $existing_default_templates,
      '#empty' => $this->t('No destinations found'),
    ];

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $entity_type
    ];

    $form['entity_bundle'] = [
      '#type' => 'hidden',
      '#value' => $bundle
    ];

    $form['entity_id'] = [
      '#type' => 'hidden',
      '#value' => $entity_id
    ];  




    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Override Default'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if (mb_strlen($form_state->getValue('message')) < 10) {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('Message should be at least 10 characters.'),
    //     );
    //   }
    // @endcode
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $data = $form_state->getValue('table');
    $entity_type = $form_state->getValue('entity_type');
    $entity_bundle = $form_state->getValue('entity_bundle');
    $entity_id = $form_state->getValue('entity_id');

    $helper_service = \Drupal::service('conditional_notification.helper');

    $helper_service->createDefaultTemplatesForOverride($data, $entity_id, $entity_type, $entity_bundle);

    $this->messenger()->addStatus($this->t('Default template now available for override.'));
  }

}
