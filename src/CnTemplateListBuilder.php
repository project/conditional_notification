<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Provides a list controller for the conditional notification store entity type.
 */
final class CnTemplateListBuilder extends EntityListBuilder {

  /**
   * Returns a query object for loading entity IDs from the storage.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query object used to load entity IDs.
   */
  protected function getEntityListQuery() : QueryInterface {

    $route_params = \Drupal::routeMatch()->getParameters();

    $query = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType
      ->getKey('id'));  

    foreach ($route_params as $param_key => $param_value) {
      if ($param_value instanceof ConfigEntityInterface) {
        $entity_type = $param_value->getEntityType()->getBundleOf();
        $entity_bundle = $param_value->id();   
        $query
          ->condition('notification_entity_type', $entity_type)
          ->condition('notification_entity_bundle', $entity_bundle);
      }
      else {
        $entity_type = $param_key;
        $entity_id = $param_value;
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
        if ($entity instanceof EntityInterface) {
          $query
            ->condition('entity_id', $entity->id())
            ->condition('notification_entity_type', $entity_type)
            ->condition('notification_entity_bundle', $entity->bundle());
        }
      } 
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query;

  }    

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\conditional_notification\CnTemplateInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}
