<?php

namespace Drupal\conditional_notification\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\eca\Event\EntityEventInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when a conditional notification condition on CREATE fired.
 *
 * @EcaEvent(
 *   id = "conditional_notification.condition_insert_trigger",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class ConditionInsertTriggerEvent extends Event {

  public const EVENT_NAME = 'conditional_notification.condition_insert_trigger';

  /**
   * The main entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;
  
  /**
   * Contains entity type of the entity.
   *
   * @var string
   */
  public string $entityType;

  /**
   * Contains bundle of the entity.
   *
   * @var string
   */
  public string $bundle;  
  
  /**
   * Contains notification type.
   *
   * @var string
   */
  public string $notificationType;

  /**
   * Contains the notification id.
   *
   * @var int
   */
  public string $notification_id;  
  
  /**
   * Contains the machine name for the eca model
   *
   * @var string
   */
  public string $ecaModelMachineName;  
  
  /**
   * The output of the processed result.
   *
   * @var mixed
   */
  public mixed $result;   
       

  /**
   * Constructs a new CnConditionEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The effected entity.
   * @param string $action_type
   *   The action type.
   * @param array $notification_type
   *   The notification type.
   * @param string listed_at
   *   The listed at.
   */
  public function __construct(EntityInterface $entity, string $entity_type, string $bundle, string $notification_type, string $notification_id, string $eca_model_machine_name, mixed $result = NULL) {
    $this->entity = $entity;
    $this->entityType = $entity_type;
    $this->bundle = $bundle;
    $this->notificationType = $notification_type;
    $this->notification_id = $notification_id;
    $this->ecaModelMachineName = $eca_model_machine_name;
    $this->result = $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  } 
 

}
