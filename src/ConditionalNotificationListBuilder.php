<?php

declare(strict_types=1);

namespace Drupal\conditional_notification;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides a listing of conditional notifications.
 */
final class ConditionalNotificationListBuilder extends ConfigEntityListBuilder {

  /**
   * Returns a query object for loading entity IDs from the storage.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query object used to load entity IDs.
   */
  protected function getEntityListQuery() : QueryInterface {

    $route_params = \Drupal::routeMatch()->getParameters();

    $query = $this->getStorage()
      ->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType
      ->getKey('id'));  

    foreach ($route_params as $param_key => $param_value) {
      if ($param_value instanceof ConfigEntityInterface) {
        $entity_type = $param_value->getEntityType()->getBundleOf();
        $entity_bundle = $param_value->id();   
        $query
          ->condition('notification_entity_type', $entity_type)
          ->condition('notification_entity_bundle', $entity_bundle);
      }
      else {
        $entity_type = $param_key;
        $entity_id = $param_value;
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
        if ($entity instanceof EntityInterface) {
          $query
            ->condition('entity_id', $entity->id())
            ->condition('notification_entity_type', $entity_type)
            ->condition('notification_entity_bundle', $entity->bundle());
        }
      } 
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query;

  }  

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Notifictation template');
    //$header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\conditional_notification\ConditionalNotificationInterface $entity */
    $row['label'] = $entity->label();
    //$row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}
