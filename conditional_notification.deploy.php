<?php

/**
 * @file
 * Post update functions for conditonal_notification.
 */

use Drupal\conditional_notification\ConditionalNotificationInterface;
use Drupal\conditional_notification\CnNotificationInterface;

/**
 * Migrate data of config entity conditional_notification 
 * into new content entity cn_template! 
 */ 
function conditional_notification_deploy_0001_migrate_config_to_content() {

  $migrated_records = 0;

  // We need to migrate config entity "conditional_notification" to content entity "cn_template".
  $cn_template_storage = \Drupal::entityTypeManager()->getStorage('cn_template');
  
  $config = \Drupal::entityTypeManager()->getStorage('conditional_notification')->loadMultiple();
  if (!empty($config)) {

    $records_config = count($config);
    $migrated_records = 0;
    $data = [];

    foreach ($config as $key => $item) {
      if ($item instanceof ConditionalNotificationInterface) {     
        $data['label'] = $item->label();
        $data['subject'] = $item->getSubject();
        $data['status'] = $item->status();
        $data['body'] = $item->getBody();
        $data['mail_from'] = $item->getMailFrom();
        $data['mail_reply_to'] = $item->getMailReplyTo();
        $data['mail_from_enabled'] = $item->getMailFromEnabled();
        $data['notification_entity_type'] = $item->getNotificationEntityType();
        $data['notification_entity_bundle'] = $item->getNotificationEntityBundle();
        $data['entity_action_type'] = $item->getEntityActionType();
        $data['notification_id'] = $item->getNotificationId();
        $data['entity_id'] = $item->getEntityId();
        $data['notification_type'] = $item->getNotificationType();
        $data['time_offset_enabled'] = $item->getTimeOffsetEnabled();
        $data['time_offset'] = $item->getTimeOffset();
        $data['time_offset_type'] = $item->getTimeOffsetType();
        $data['time_offset_date_field'] = $item->getTimeOffsetDateField();
        $data['time_offset_date_field_part'] = $item->getTimeOffsetDateFieldPart();
        $data['notification_context_eca'] = $item->getNotificationContextEca();
        $data['notification_condition_eca'] = $item->getNotificationConditionEca();
        
        if (!empty($data)) {
        
          try {
            $entity = $cn_template_storage->create($data);
            $entity->save();
            $migrated_records++;
          }
          catch (\Exception $exception) {
            \Drupal::logger('conditonal_notification')->warning('Problem migrating config entity record into content entity!');
          }
        }
      }
    }
  }

  return t('Migrated @migrated_records config entity entries!', ['@migrated_records' => $migrated_records]);

}




